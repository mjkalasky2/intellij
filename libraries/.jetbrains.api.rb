require 'net/http'
require 'json'

module IntelliJ
  module API
    def request_json_response(url)
      uri = URI(url)
      response = Net::HTTP.get_response(uri)
      status = response.code.to_i
      Chef::Log.debug("Response from #{url} (status: #{status}):\n#{response.body}")
      raise IntelliJ::Exceptions::GeneralApiError, response if status >= 400
      JSON.parse(response.body)
    end

    def intellij_releases(edition)
      e_code = edition_code(edition)
      response = request_json_response("https://data.services.jetbrains.com/products/releases?code=#{e_code}&type=release&_=#{Time.now.getutc}")
      releases = response[e_code] || []
      result = {}
      releases.each do |release|
        result[release['version']] = release
      end
      result
    end

    def intellij_release_data(version, edition)
      releases = intellij_releases(edition)
      version ||= releases.keys[0]
      releases[version]
    end

    def release_download_info(release)
      os = Chef::Platform.windows? ? 'windowsZip' : 'linux'
      info = release['downloads'][os]
      [info['link'], download_checksum_from_link(info['checksumLink'])]
    end

    def download_checksum_from_link(link)
      uri = URI(link)
      response = Net::HTTP.get(uri)
      response.split(' ').first
    end

    def suggest_plugin(name, edition)
      product = product_edition_code(edition)
      response = request_json_response("https://plugins.jetbrains.com/api/searchSuggest?isIDERequest=false&product=#{product}&term=#{name}")
      result = {}
      response.each do |suggestion|
        name = suggestion['value']
        id = suggestion['url'].match(/\/plugin\/(\d+)-.*/)[1]
        result[name] = {}
        result[name]['id'] = id
        result[name]['name'] = name
        result[name]['family'] = suggestion['target']
      end
      result
    end

    def plugin_updates(id, family)
      response = request_json_response("https://plugins.jetbrains.com/api/plugins/#{id}/updates")
      result = {}
      response.each do |update|
        result[update['id']] = update.merge({})
        result[update['id']]['source'] = "https://plugins.jetbrains.com/files/#{update['file']}?updateId=#{update['id']}&pluginId=#{id}&family=#{family}"
      end
      result
    end
  end
end
