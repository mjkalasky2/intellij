module IntelliJ
  module Workspace
    module AppServer
      def build_xml(server_name, type, path)
        <<-EOH
        <ServerDK NAME="#{server_name}">
          <option name="SOURCE_INTEGRATION_NAME" value="#{type} Server" />
          <DATA>
            #{generate_server_data(type, path)}
          </DATA>
        </ServerDK>
        EOH
      end

      def generate_server_data(type, path)
        case type
        when :JBoss
          <<-EOH
            <option name="HOME" value="#{path}" />
          EOH
        when :Tomcat
          <<-EOH
            <option name="CATALINA_HOME" value="#{path}" />
            <option name="CATALINA_BASE" value="" />
          EOH
        end
      end

      def build_server_libraries(server_name)
        <<-EOH
          <library name="#{server_name}">
            <CLASSES />
            <JAVADOC />
            <SOURCES />
          </library>
        EOH
      end
    end
  end
end
