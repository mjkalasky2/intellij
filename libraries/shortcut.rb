include IntelliJ::Helpers

module IntelliJ
  module Shortcut
    def default_shortcut_name(intellij_path)
      version, edition_code, = local_intellij_info(intellij_path)

      if version
        editions = { IC: 'Community', IU: 'Ultimate' }
        "IntelliJ IDEA #{editions[edition_code.to_sym]} edition #{version}"
      else
        'IntellJ IDEA'
      end
    end

    def default_shortcut_directory(user)
      if Chef::Platform.windows?
        "#{Dir.home(user)}/Desktop"
      else
        "#{Dir.home(user)}/.local/share/applications"
      end
    end

    def windows_shortcut_target(properties, intellij_path)
      if properties
        'C:\Windows\System32\cmd.exe'
      else
        "#{intellij_path}/bin/idea64.exe"
      end
    end

    def windows_shortcut_arguments(properties, intellij_path)
      if properties
        "/c \"SET ^\"IDEA_PROPERTIES=#{properties}^\" && START /D ^\"#{intellij_path}/bin^\" idea64.exe\""
      else
        ''
      end
    end
  end
end
