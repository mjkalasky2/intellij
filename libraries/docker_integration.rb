module IntelliJ
  module DockerIntegration
    def build_server_xml(api_url, shared_directories, server_name, machine_name)
      api_url ||= default_api_url(machine_name)
      path_mappings = server_path_mappings(shared_directories)

      options = { apiUrl: api_url }
      options['certificatesPath'] = "#{Dir.home}/.docker/machine/machines/#{machine_name}" if machine_name
      options['machine'] = machine_name if machine_name

      opts = options.map { |k, v| "<option name=\"#{k}\" value=\"#{v}\" />" }

      <<-EOH
          <remote-server name="#{server_name}" type="docker">
            <configuration>
              #{opts.join}
              #{path_mappings}
            </configuration>
          </remote-server>
      EOH
    end

    def server_path_mappings(shared_directories)
      result = nil
      if shared_directories && !shared_directories.empty?
        mappings = []
        shared_directories.each do |local_path, docker_path|
          mappings.push(<<-EOH)
            <DockerPathMappingImpl>
              <option name="localPath" value="#{local_path}" />
              <option name="virtualMachinePath" value="#{docker_path}" />
            </DockerPathMappingImpl>
          EOH
        end
        result = <<-EOH
          <option name="pathMappings">
            <list>
              #{mappings.join}
            </list>
          </option>
        EOH
      end
      result
    end

    # Values for each system defined here: https://www.jetbrains.com/help/idea/docker-connection-settings.html
    def default_api_url(machine_name)
      if machine_name
        'https://192.168.99.100:2376'
      elsif Chef::Platform.windows?
        # platform_data = Chef::Platform.find_platform_and_version(node) - can be used to test for Windows LCOW
        # Chef::ReservedNames::Win32::Version.new with 'chef/win32/version' can be also useful
        'tcp://localhost:2375'
      else
        'unix:///var/run/docker.sock'
      end
    end

    def generate_bulk_registries(registries)
      edits = []
      registries.each do |name, properties|
        edits.push(
          target: "/application/component[@name='DockerRegistry']/DockerRegistry[option[@name='name'][@value='#{name}']]",
          action: :append_if_missing,
          parent: '/application/component[@name="DockerRegistry"]',
          fragment: generate_registry_xml(name, properties)
        )
      end
      edits
    end

    def remove_bulk_registries(registries)
      edits = []
      registries.each do |name, _|
        edits.push(
          target: "/application/component[@name='DockerRegistry']/DockerRegistry[option[@name='name'][@value='#{name}']]",
          action: :remove
        )
      end
      edits
    end

    def generate_registry_xml(name, properties)
      options = []
      options.push("<option name=\"address\" value=\"#{properties[:address]}\" />")
      options.push("<option name=\"name\" value=\"#{name}\" />")
      options.push("<option name=\"username\" value=\"#{properties[:username]}\" />") if properties[:username]

      params = []
      params.push("email=\"#{properties[:email]}\"") if properties[:email]
      <<-EOH
          <DockerRegistry #{params.join(' ')}>
            #{options.join}
          </DockerRegistry>
      EOH
    end

    def load_existing_config(server_name, workspace_path)
      base_data = load_server_data(server_name, workspace_path)
      base_data[:registries] = load_registries_data(workspace_path)
      base_data
    end

    def load_server_data(server_name, workspace_path)
      file = "#{workspace_path}/config/options/remote-servers.xml"
      result = {
        name: server_name,
        api_url: nil,
        machine_name: nil,
        shared_directories: {},
      }
      if ::File.exist?(file)
        xpath = "/application/component[@name=\"RemoteServers\"]/remote-server[@name=\"#{server_name}\"][@type=\"docker\"]"
        server = process_xpath_in_xml(file, xpath)
        result[:api_url] = server&.at_xpath('configuration/option[@name="apiUrl"]')&.attr('value')
        server&.xpath('configuration/option[@name="pathMappings"]/list/DockerPathMappingImpl').each do |mapping|
          local = mapping.at_xpath('option[@name="localPath"]')&.attr('value').to_sym
          docker = mapping.at_xpath('option[@name="virtualMachinePath"]')&.attr('value')
          result[:shared_directories][local] = docker
        end
      end
      result
    end

    def load_registries_data(workspace_path)
      file = "#{workspace_path}/config/options/docker-registry.xml"
      result = {}
      if ::File.exist?(file)
        xpath = "/application/component[@name='DockerRegistry']/DockerRegistry"
        registries = process_xpath_in_xml(file, xpath)

        registries&.each do |registry|
          name = registry.at_xpath('option[@name="name"]')&.attr('value').to_sym
          result[name] = {
            address: registry.at_xpath('option[@name="address"]')&.attr('value'),
            username: registry.at_xpath('option[@name="username"]')&.attr('value'),
            email: registry.attr('email'),
          }
        end
      end
      result
    end

    def hash_sym_to_s(hash)
      Hash[ hash.map { |k, v| [k.to_sym, v] } ]
    end
  end
end
