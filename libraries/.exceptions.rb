module IntelliJ
  module Exceptions
    class GeneralApiError < StandardError
      def initialize(response)
        message = "The API '#{response.uri}' returned [#{response.code}] #{response.message}:\n#{response.body}"
        super(message)
      end
    end

    class IntelliJVersionNotFound < StandardError
      def initialize(version, edition)
        message = "The IntelliJ IDEA with version: '#{version}' and edition '#{edition}' could not be found in the Jetbrains repository."
        super(message)
      end
    end

    class IntelliJPluginNotFound < StandardError
      def initialize(name, edition, similar)
        message = "The plugin #{name} for the IntelliJ #{edition} edition could not be found."
        if similar
          suggestions = similar.join('", "')
          message << "\nMaybe you want to install one of: \"#{suggestions}\"?"
        end
        super(message)
      end
    end

    class IntelliJNotFound < StandardError
      def initialize(path)
        message = "The IntelliJ was not found in the path '#{path}'!"
        super(message)
      end
    end

    class IntelliJPluginVersionNotFound < StandardError
      def initialize(name, version, updates)
        message = "The version '#{version}' for the IntelliJ '#{name}' plugin could not be found."
        if updates
          suggestions = updates.join('", "')
          message << "\nMaybe you want to install one of: \"#{suggestions}\"?"
        end
        super(message)
      end
    end

    class IntelliJArchiveTypeNotSupported < StandardError
      def initialize(extension)
        message = "The '#{extension}' type of archive is currently not supported. Please use .tar.gz or .zip files."
        super(message)
      end
    end
  end
end
