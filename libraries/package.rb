include IntelliJ::API
require 'json'

module IntelliJ
  module Install
    def self.included(base)
      base.extend self
    end

    def summarize_release(params)
      source = params.source
      checksum = params.checksum
      version = params.version

      unless source
        release = intellij_release_data(version, params.edition)
        raise IntelliJ::Exceptions::IntelliJVersionNotFound.new(version, params.edition) unless release
        version = release['version']
        source, checksum = release_download_info(release)
      end

      [source, checksum, version]
    end

    def intellij_installed?(path)
      ::File.exist?("#{path}/build.txt")
    end
  end
end
