require 'nokogiri'
require 'zip'
require 'pathname'

module IntelliJ
  module Helpers
    def self.included(base)
      base.extend self
    end

    def detect_vcs(path)
      vcs_type = nil
      vcs_type = :Git if ::File.exist?("#{path}/.git")
      vcs_type
    end

    def create_missing_component(name, file, fragment = nil, parent = 'project')
      xml_edit file do
        parent "/#{parent}"
        target "/#{parent}/component[@name=\"#{name}\"]"
        fragment "<component name=\"#{name}\">#{fragment}</component>"
        action :append_if_missing
        only_if { ::File.exist?(file) }
        not_if { ::File.read(file).match(/<component.*name="#{name}".*>/) }
      end
    end

    def process_xpath_in_xml(file, xpath)
      result = nil
      if ::File.exist?(file)
        doc = Nokogiri::XML(File.read(file), &:noblanks)
        namespace = doc.root.namespaces
        result = doc.xpath(xpath, namespace)
      end
      result
    end

    def xpath_exists?(file, xpath)
      !process_xpath_in_xml(file, xpath).to_s.empty?
    end

    def pretty_format_xml(xml, indent = 2)
      Nokogiri::XML(xml, &:noblanks).root.to_xml(indent: indent)
    end

    def get_tmp_path(filename)
      "#{Dir.tmpdir}/#{::File.basename(filename)}"
    end

    def edition_from_code(code)
      editions = { IU: :ultimate, IC: :community }
      editions[code.to_sym]
    end

    def edition_short_code(edition)
      editions = { ultimate: 'IU', community: 'IC' }
      editions[edition.to_sym]
    end

    def edition_code(edition)
      editions = { ultimate: 'IIU', community: 'IIC' }
      editions[edition.to_sym]
    end

    def product_edition_code(edition)
      editions = { ultimate: 'idea', community: 'idea_ce' }
      editions[edition.to_sym]
    end

    def local_intellij_info(path)
      result = nil
      file = "#{path}/product-info.json"
      if ::File.exist?(file)
        info = JSON.parse(::File.read(file))
        result = [info['version'], info['productCode'], info['buildNumber']]
      end
      result
    end

    def project_relative_path(path, project_path)
      relative_path(project_path, path, '$PROJECT_DIR$')
    end

    def map_to_xml_attributes(map)
      result = []
      map.each do |key, value|
        result.push("#{key}=\"#{value}\"") unless value.to_s.empty?
      end
      result.join(' ')
    end

    def zip_contents(file)
      result = []
      if ::File.exist?(file)
        Zip::File.open(file) do |zip_file|
          zip_file.each do |entry|
            result.push(entry.name)
          end
        end
      end
      result
    end

    def relative_path(from, to, prefix = nil)
      begin
        path = Pathname.new(to).relative_path_from(Pathname.new(from)).to_s
        "#{prefix}/#{path}"
      rescue ArgumentError => e
        raise unless e.to_s.start_with?('different prefix:')
        to
      end
    end

    def version_earlier?(first, second)
      first, second = normalize_versions(first, second)
      Gem::Version.new(first) <= Gem::Version.new(second)
    end

    def normalize_versions(first, second)
      first_versions = first.split('.')
      second_versions = second.split('.')

      first_id = first_versions.find_index('*')
      second_id = second_versions.find_index('*')
      minimal = first_id && second_id ? [first_id, second_id].min : first_id || second_id

      first_versions = drop_last_elements(first_versions, first_versions.length - minimal) if minimal
      second_versions = drop_last_elements(second_versions, second_versions.length - minimal) if minimal

      [first_versions.join('.'), second_versions.join('.')]
    end

    def drop_last_elements(array, amount)
      amount = array.length if amount > array.length
      array.reverse.drop(amount).reverse
    end
  end
end
