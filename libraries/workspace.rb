module IntelliJ
  module Workspace
    def generate_recent_projects_bulk(project_path)
      [
        {
          action: :append_if_missing,
          parent: '/application/component[@name="RecentProjectsManager"]',
          target: '/application/component[@name="RecentProjectsManager"]/option[@name="openPaths"]',
          fragment: <<-EOH,
            <option name="openPaths">
              <list>
                <option value="#{project_path}" />
              </list>
            </option>
          EOH
        },
        {
          action: :append_if_missing,
          parent: '/application/component[@name="RecentProjectsManager"]',
          target: '/application/component[@name="RecentProjectsManager"]/option[@name="lastPath"]',
          fragment: "<option name=\"lastPath\" value=\"#{project_path}\" />",
        },
      ]
    end

    def get_color_class(color_scheme)
      classes = {
        Darcula: '<laf class-name="com.intellij.ide.ui.laf.darcula.DarculaLaf" />',
        IntelliJ: '<laf class-name="com.intellij.ide.ui.laf.IntelliJLaf" />',
        'High contrast': '<laf class-name="com.intellij.ide.ui.laf.darcula.DarculaLaf" themeId="JetBrainsHighContrastTheme" />',
      }
      classes[color_scheme.to_sym]
    end
  end
end
