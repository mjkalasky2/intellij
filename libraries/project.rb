module IntelliJ
  module Project
    def self.included(base)
      base.extend self
    end

    def get_default_sdk_fragment(sdk_name, sdk_type, lang_level)
      result = "<component name=\"ProjectRootManager\" version=\"2\" languageLevel=\"#{lang_level}\" default=\"false\"/>"
      if sdk_name
        result = case sdk_type
                 when :JavaSDK
                   <<-EOH
                      <component name="ProjectRootManager" version="2" languageLevel="#{lang_level}" default="false" project-jdk-name="#{sdk_name}" project-jdk-type="JavaSDK">
                        <output url="file://$PROJECT_DIR$/out" />
                      </component>
                   EOH
                 else
                   "<component name=\"ProjectRootManager\" version=\"2\" languageLevel=\"#{lang_level}\" default=\"false\" project-jdk-name=\"#{sdk_name}\" project-jdk-type=\"#{sdk_type}\" />"
                 end
      end
      result
    end
  end
end
