module IntelliJ
  module Workspace
    module RunConfiguration
      def self.included(base)
        base.extend self
      end

      def generate_run_config_xml(attributes, body)
        <<-EOH
          <configuration #{map_to_xml_attributes(attributes)}>
            #{body}
          </configuration>
        EOH
      end

      def update_manager_selection(file, selected)
        run_manager = process_xpath_in_xml(file, '/project/component[@name="RunManager"]').first
        run_manager['selected'] = selected
        run_manager.to_s
      end
    end
  end
end
