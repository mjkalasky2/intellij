include IntelliJ::Helpers

module IntelliJ
  module Module
    def self.included(base)
      base.extend self
    end

    def maven_module?(path)
      ::File.exist?("#{path}/pom.xml")
    end

    def generate_module_file_path(path, name, dir)
      name ||= ::File.basename(path)
      parent = dir || path
      content_location = dir ? relative_path(dir, path, '$MODULE_DIR$') : '$MODULE_DIR$'
      file_location = "#{parent}/#{name}.iml"
      [file_location, content_location]
    end
  end
end
