require 'nokogiri'

module IntelliJ
  module Workspace
    module Artifact
      def self.included(base)
        base.extend self
      end

      def build_xml(name, type, output_path, root, project_path)
        filename = generate_artifact_filename(name)
        output_path ||= "$PROJECT_DIR$/out/artifacts/#{filename}"
        relative_output = output_path.gsub(project_path, '$PROJECT_DIR$')
        attrs = { name: name }
        attrs['type'] = type if type

        <<-EOH
          <component name="ArtifactManager">
            <artifact #{map_to_xml_attributes(attrs)}>
              <output-path>#{relative_output}</output-path>
              #{root}
            </artifact>
          </component>
        EOH
      end

      def get_artifact_path(project_path, artifact_name)
        repo = "#{project_path}/.idea/artifacts"
        filename = generate_artifact_filename(artifact_name)
        "#{repo}/#{filename}.xml"
      end

      def generate_artifact_filename(artifact_name)
        splits = artifact_name.split(/[\s\-:.]/)
        splits.join('_')
      end
    end
  end
end
