include IntelliJ::Helpers

module IntelliJ
  module Workspace
    module Changelist
      def generate_files_list(files, project_path)
        files = [*files]
        result = []
        files.each do |file|
          result.push(file.gsub(project_path, '$PROJECT_DIR$'))
        end
        result
      end

      def generate_xml(data)
        changes = []
        data[:files].each do |change|
          changes.push("<change beforePath=\"#{change}\" afterDir=\"false\" />") unless change.to_s.empty?
        end
        attributes = {}
        attributes['id'] = data[:id] if data[:id]
        attributes['default'] = true if data[:default]
        attributes['name'] = data[:name]
        attributes['comment'] = data[:comment] if data[:comment]
        attrs = attributes.map { |k, v| "#{k}=\"#{v}\"" }.join(' ')
        <<-EOH
          <list #{attrs}>
            #{changes.join("\n")}
          </list>
        EOH
      end

      def generate_bulk_actions(files, changelist_name, xml)
        result = []
        files.each do |file|
          after_path = "/project/component[@name=\"ChangeListManager\"]/list/change[@afterPath=\"#{file}\"]"
          before_path = "/project/component[@name=\"ChangeListManager\"]/list/change[@beforePath=\"#{file}\"]"

          result.push(
            action: :remove,
            target: "#{after_path}|#{before_path}"
          )
        end
        result.push(
          action: :append_if_missing,
          parent: '/project/component[@name="ChangeListManager"]',
          target: "/project/component[@name=\"ChangeListManager\"]/list[@name=\"#{changelist_name}\"]",
          fragment: xml
        )
        result
      end

      def get_changelist_data(project_path, changelist_name)
        workspace_file = "#{project_path}/.idea/workspace.xml"
        xpath = "/project/component[@name=\"ChangeListManager\"]/list[@name=\"#{changelist_name}\"]"
        changelist = process_xpath_in_xml(workspace_file, xpath)
        if changelist.to_s.empty?
          {
            id: nil,
            default: nil,
            comment: nil,
            files: [],
            name: changelist_name,
          }
        else
          files = []
          changelist&.children&.each do |change|
            path = change.attr('afterPath') || change.attr('beforePath')
            files.push(path)
          end
          {
            id: changelist&.attr('id'),
            default: changelist&.attr('default'),
            comment: changelist&.attr('comment'),
            files: files,
            name: changelist_name,
          }
        end
      end

      def default_manager
        <<-EOH
          <option name="EXCLUDED_CONVERTED_TO_IGNORED" value="true" />
          <option name="SHOW_DIALOG" value="false" />
          <option name="HIGHLIGHT_CONFLICTS" value="true" />
          <option name="HIGHLIGHT_NON_ACTIVE_CHANGELIST" value="false" />
          <option name="LAST_RESOLUTION" value="IGNORE" />
        EOH
      end
    end
  end
end
