project_path = node['intellij']['example']['project']
user = node['intellij']['example']['user']
group = node['intellij']['example']['group']

group group if group

user user do
  group group
end if user

# Creating sample Maven project with simple HelloWorld Java class
# We assume that it is already existing and we just want to load it into the IntelliJ project
# We will also assume that this module is taken from Git
directory "#{project_path}/Module2/.git" do
  recursive true
  action :create
  user user
  group group
end

file "#{project_path}/Module2/pom.xml" do
  content <<-EOH
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.thedeem.sample</groupId>
    <artifactId>sample-maven-module</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>
    <properties>
       <maven.compiler.source>1.8</maven.compiler.source>
       <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
    <dependencies>
    </dependencies>
</project>
  EOH
  action :create
  user user
  group group
end

directory "#{project_path}/Module2/src/main/java/org.thedeem.sample" do
  recursive true
  action :create
  user user
  group group
end

file "#{project_path}/Module2/src/main/java/org.thedeem.sample/HelloWorld.java" do
  content <<-EOH
package org.thedeem.sample;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World");
    }
}
  EOH
  action :create
  user user
  group group
end

directory "#{project_path}/Module1" do
  recursive true
  action :create
  user user
  group group
end

file "#{project_path}/Module1/example.bat" do
  content <<-EOH
@echo off
SET PARAMETER=%1
echo "Our script got parameter: %PARAMETER%"
  EOH
  action :create
end
