#
# This recipe is only an example for configuring IntelliJ.
# It will install the latest version of IntelliJ Community Edition, then will configure new workspace (which is great way
# to separate different configurations like plugins, SDKs etc.), add project, modules and their configurations
# Please note that this recipe DOES NOT install Git/Maven/Java etc. This cookbook only manages the IntelliJ installation
# and hence expects all those to be installed in the first place.
#

intellij_path = node['intellij']['example']['path']
workspace_path = node['intellij']['example']['workspace']
project_path = node['intellij']['example']['project']
user = node['intellij']['example']['user']
group = node['intellij']['example']['group']

intellij intellij_path do
  edition :community
  user user
  group group
  action :install
end

intellij_workspace workspace_path do
  default_project project_path
  intellij_path intellij_path
  color_scheme :Darcula
  user user
  group group
  action :create
end

intellij_plugin 'Batch Scripts Support' do
  workspace workspace_path
  intellij_path intellij_path
  edition :community
  user user
  group group
  action :install
end

intellij_sdk 'JDK 10.0' do
  workspace workspace_path
  path node['intellij']['example']['java_path']
  type :JavaSDK
  user user
  group group
  action :add
end

intellij_project project_path do
  project_name 'My fantastic project'
  default_sdk_name 'JDK 10.0'
  default_sdk_type :JavaSDK
  user user
  group group
  action :create
end

intellij_module "#{project_path}/Module1" do
  project_path project_path
  module_name 'Created new module'
  user user
  group group
  config_directory "#{project_path}/configs"
  action :create
end

intellij_module "#{project_path}/Module2" do
  project_path project_path
  type :JAVA_MODULE
  module_name 'sample-maven-module'
  sdk_name 'JDK 10.0'
  sdk_type :JavaSDK
  user user
  group group
  action :create
end

intellij_run_configuration 'Clean install Maven module' do
  project_path project_path
  type 'MavenRunConfiguration'
  factory 'Maven'
  folder 'Java Application'
  body <<-EOH
    <MavenSettings>
      <option name="myGeneralSettings" />
      <option name="myRunnerSettings" />
      <option name="myRunnerParameters">
        <MavenRunnerParameters>
          <option name="profiles">
            <set />
          </option>
          <option name="goals">
            <list>
              <option value="clean" />
              <option value="install" />
            </list>
          </option>
          <option name="pomFileName" />
          <option name="profilesMap">
            <map />
          </option>
          <option name="resolveToWorkspace" value="false" />
          <option name="workingDirPath" value="$PROJECT_DIR$/Module2" />
        </MavenRunnerParameters>
      </option>
    </MavenSettings>
    <method v="2" />
  EOH
  user user
  group group
  action :create
end

intellij_run_configuration 'HelloWorld from Java' do
  project_path project_path
  type 'Application'
  factory 'Application'
  folder 'Java Application'
  body <<-EOH
    <option name="MAIN_CLASS_NAME" value="org.thedeem.sample.HelloWorld" />
    <module name="sample-maven-module" />
    <method v="2">
      <option name="Make" enabled="true" />
    </method>
  EOH
  user user
  group group
  action :create
end

intellij_run_configuration 'Start simple batch script' do
  project_path project_path
  type 'BatchConfigurationType'
  factory 'Batch'
  body <<-EOH
    <option name="INTERPRETER_OPTIONS" value="" />
    <option name="WORKING_DIRECTORY" value="$PROJECT_DIR$" />
    <option name="PARENT_ENVS" value="true" />
    <module name="" />
    <option name="SCRIPT_NAME" value="$PROJECT_DIR$/Module1/example.bat" />
    <option name="PARAMETERS" value="Hello" />
    <method v="2" />
  EOH
  user user
  group group
  action :create
end

intellij_artifact 'sample-maven-module:jar' do
  project_path project_path
  type 'jar'
  root <<-EOH
    <root id="archive" name="sample-maven-module.jar">
      <element id="module-output" name="sample-maven-module" />
    </root>
  EOH
  user user
  group group
  action :create
end

intellij_changelist 'Do NOT commit those' do
  project_path project_path
  comment 'Any changes in those files should not be committed'
  files %W(#{project_path}/Module1/example.bat)
  user user
  group group
  action :create
end

intellij_plugin 'Docker' do
  workspace workspace_path
  intellij_path intellij_path
  edition :community
  user user
  group group
  action :install
end

intellij_docker_integration 'Local Docker' do
  workspace_path workspace_path
  registries(
    'Docker Registry': {
      address: 'registry.hub.docker.com',
      username: 'myuser',
      email: 'myemail@gmail.com',
    }
  )
  shared_directories(
    project_path => '/home/myuser/data'
  )
  user user
  group group
end

intellij_application_server 'Local Apache Tomcat' do
  type :Tomcat
  path node['intellij']['example']['tomcat_path']
  user user
  group group
  workspace_path workspace_path
end

intellij_application_server 'Local WildFly' do
  type :JBoss
  path node['intellij']['example']['jboss_path']
  user user
  group group
  workspace_path workspace_path
end

intellij_shortcut intellij_path do
  user user
  group group
end

intellij_shortcut intellij_path do
  idea_properties "#{workspace_path}/config/idea.properties"
  shortcut_name 'My shortcut'
  directory "/home/#{user}"
  user user
  group group
end
