intellij node['intellij']['recipe']['path'] do
  action :update
  source node['intellij']['recipe']['source']
  checksum node['intellij']['recipe']['checksum']
  edition node['intellij']['recipe']['edition']
  version node['intellij']['recipe']['version']
  user node['intellij']['recipe']['user']
  group node['intellij']['recipe']['group']
end
