# Testing

We would like to have this cookbook meeting all the community standards to ensure the highest cookbook quality.
The cookbook is using the `delivery` (and whole ChefDK) tool to test the code style and the logic itself.

If you want to read more information about the testing, please visit the general TESTING file that applies for all Chef
cookbooks:

https://github.com/chef-cookbooks/community_cookbook_documentation/blob/master/TESTING.MD

## Special notes

Although the rspec and kitchen tests are implemented, they can not test the whole functionality. This cookbook configures
the IntelliJ, which is mostly focused on the GUI. The proper testing of the cookbook should cover opening the IntelliJ
and opening all the updated settings views.

Before each release, the cookbook requires manual regression testing. 
