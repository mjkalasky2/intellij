require 'spec_helper'
require_relative '../../../../intellij/libraries/artifact'

describe 'intellij_artifact' do
  step_into :intellij_artifact
  platform 'ubuntu'

  describe 'testing the resource' do
    context 'creating the artifact' do
      recipe do
        intellij_artifact 'sample-maven-module:jar' do
          project_path '/home/user/myproject'
          type 'jar'
          root <<-EOH
            <root id="archive" name="sample-maven-module.jar">
              <element id="module-output" name="sample-maven-module" />
            </root>
          EOH
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      it { is_expected.to create_intellij_artifact('sample-maven-module:jar') }
      it { is_expected.to create_directory('/home/user/myproject/.idea/artifacts').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_file('/home/user/myproject/.idea/artifacts/sample_maven_module_jar.xml').with(user: 'myuser', group: 'mygroup') }
    end

    context 'removing the artifact' do
      recipe do
        intellij_artifact 'sample-maven-module:jar' do
          project_path '/home/user/myproject'
          action :delete
        end
      end

      it { is_expected.to delete_intellij_artifact('sample-maven-module:jar') }
      it { is_expected.to delete_file('/home/user/myproject/.idea/artifacts/sample_maven_module_jar.xml') }
    end
  end

  describe 'testing the artifact library' do
    before do
      class ArtifactLibrary
        include IntelliJ::Workspace::Artifact
      end
    end

    context 'generating artifact file name' do
      it 'generates the proper artifact file name' do
        artifact_name = 'some-artifact with.weird:symbols'
        expected_filename = 'some_artifact_with_weird_symbols'

        result = ArtifactLibrary.generate_artifact_filename(artifact_name)

        expect(result).to eq(expected_filename)
      end
    end

    context 'building the xml for the artifact' do
      let(:type) { 'jar' }
      let(:artifact_name) { 'some-artifact with.weird:symbols' }
      let(:root) { '<root/>' }

      it 'generates full artifact xml and sets up the relative output' do
        output = '/home/user/myproject/artifacts-output'
        expected_xml = <<-EOH
          <component name="ArtifactManager">
            <artifact name="#{artifact_name}" type="#{type}">
              <output-path>$PROJECT_DIR$/artifacts-output</output-path>
              #{root}
            </artifact>
          </component>
        EOH

        result = ArtifactLibrary.build_xml(artifact_name, type, output, root, '/home/user/myproject')

        # Remove useless spaces
        expected_xml.gsub!(/\s+/, ' ')
        result.gsub!(/\s+/, ' ')

        expect(result).to eq(expected_xml)
      end

      it 'sets up default output if it is not set' do
        expected_xml = <<-EOH
          <component name="ArtifactManager">
            <artifact name="#{artifact_name}" type="#{type}">
              <output-path>$PROJECT_DIR$/out/artifacts/some_artifact_with_weird_symbols</output-path>
              #{root}
            </artifact>
          </component>
        EOH

        result = ArtifactLibrary.build_xml(artifact_name, type, nil, root, '/home/user/myproject')

        # Remove useless spaces
        expected_xml.gsub!(/\s+/, ' ')
        result.gsub!(/\s+/, ' ')

        expect(result).to eq(expected_xml)
      end
    end

    context 'generating the artifact path' do
      it 'returns path to the artifact' do
        artifact_name = 'some-artifact with.weird:symbols'
        expected_path = '/home/user/myproject/.idea/artifacts/some_artifact_with_weird_symbols.xml'

        result = ArtifactLibrary.get_artifact_path('/home/user/myproject', artifact_name)

        expect(result).to eq(expected_path)
      end
    end
  end
end
