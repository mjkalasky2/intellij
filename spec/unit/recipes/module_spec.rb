require 'spec_helper'

describe 'intellij_module' do
  describe 'testing the resource' do
    step_into :intellij_module
    platform 'ubuntu'

    context 'creating the module' do
      recipe do
        intellij_module '/home/user/myproject/Module2' do
          project_path '/home/user/myproject'
          type :JAVA_MODULE
          module_name 'sample-maven-module'
          sdk_name 'JDK 10.0'
          sdk_type :JavaSDK
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        mock_maven_module('/home/user/myproject/Module2')
        mock_git_module('/home/user/myproject/Module2')
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myproject/.idea/misc.xml').and_return '<project/>'
      end

      it { is_expected.to create_intellij_module('/home/user/myproject/Module2') }
      it { is_expected.to create_directory('/home/user/myproject/Module2').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/Module2/sample-maven-module.iml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_directory('/home/user/myproject/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/.idea/modules.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/.idea/vcs.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/.idea/misc.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/modules.xml')
          .with(target: '/project/component[@name="ProjectModuleManager"]/modules/module[@filepath="$PROJECT_DIR$/Module2/sample-maven-module.iml"]')
      end
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/vcs.xml')
          .with(target: '/project/component[@name="VcsDirectoryMappings"]/mapping[@directory="$PROJECT_DIR$/Module2"]')
      end
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/misc.xml')
          .with(target: '/project/component[@name="MavenProjectsManager"]/option/list/option[@value="$PROJECT_DIR$/Module2/pom.xml"]')
      end
    end

    context 'creating the module with external config directory' do
      recipe do
        intellij_module '/home/user/myproject/Module2' do
          project_path '/home/user/myproject'
          config_directory '/home/user/project-configuration'
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        mock_maven_module('/home/user/myproject/Module2')
        mock_git_module('/home/user/myproject/Module2')
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myproject/.idea/misc.xml').and_return '<project/>'
      end

      it { is_expected.to create_intellij_module('/home/user/myproject/Module2') }
      it { is_expected.to create_directory('/home/user/project-configuration').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/project-configuration/Module2.iml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_directory('/home/user/myproject/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/.idea/modules.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/.idea/vcs.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/.idea/misc.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/modules.xml')
          .with(target: '/project/component[@name="ProjectModuleManager"]/modules/module[@filepath="$PROJECT_DIR$/../project-configuration/Module2.iml"]')
      end
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/vcs.xml')
          .with(target: '/project/component[@name="VcsDirectoryMappings"]/mapping[@directory="$PROJECT_DIR$/Module2"]')
      end
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/misc.xml')
          .with(target: '/project/component[@name="MavenProjectsManager"]/option/list/option[@value="$PROJECT_DIR$/Module2/pom.xml"]')
      end
    end

    context 'deleting the module' do
      recipe do
        intellij_module '/home/user/myproject/Module2' do
          project_path '/home/user/myproject'
          module_name 'sample-maven-module'
          action :delete
        end
      end

      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myproject/.idea/modules.xml').and_return true
        allow(File).to receive(:exist?).with('/home/user/myproject/.idea/vcs.xml').and_return true
        allow(File).to receive(:exist?).with('/home/user/myproject/.idea/misc.xml').and_return true
      end

      it { is_expected.to delete_intellij_module('/home/user/myproject/Module2') }
      it { is_expected.to delete_file('/home/user/myproject/Module2/sample-maven-module.iml') }
      it do
        is_expected.to remove_xml('/home/user/myproject/.idea/modules.xml')
          .with(target: '/project/component[@name="ProjectModuleManager"]/modules/module[@filepath="$PROJECT_DIR$/Module2/sample-maven-module.iml"]')
      end
      it do
        is_expected.to remove_xml('/home/user/myproject/.idea/vcs.xml')
          .with(target: '/project/component[@name="VcsDirectoryMappings"]/mapping[@directory="$PROJECT_DIR$/Module2"]')
      end
      it do
        is_expected.to remove_xml('/home/user/myproject/.idea/misc.xml')
          .with(target: '/project/component[@name="MavenProjectsManager"]/option/list/option[@value="$PROJECT_DIR$/Module2/pom.xml"]')
      end
    end
  end

  describe 'testing the package library' do
    before do
      class ModuleLibrary
        include IntelliJ::Module
      end

      context 'generating the module name' do
        it 'should use provided name' do
          name = 'sample-maven-module'
          expected_result = '/home/user/myproject/Module2/sample-maven-module.iml'

          result = PackageLibrary.generate_module_file_path('/home/user/myproject/Module2', name)

          expect(result).to eq(expected_result)
        end

        it 'create a name from the directory if no name is provided' do
          name = nil
          expected_result = '/home/user/myproject/Module2/Module2.iml'

          result = PackageLibrary.generate_module_file_path('/home/user/myproject/Module2', name)

          expect(result).to eq(expected_result)
        end
      end

      context 'checking if the module is maven module' do
        it 'returns true when it does exist' do
          allow(File).to receive(:exist?).with(anything).and_call_original
          allow(File).to receive(:exist?).with('/home/user/myproject/Module2/pom.xml').and_return true

          result = PackageLibrary.maven_module?('/home/user/myproject/Module2')

          expect(result).to eq(true)
        end

        it 'returns false when it does not exist' do
          allow(File).to receive(:exist?).with(anything).and_call_original
          allow(File).to receive(:exist?).with('/home/user/myproject/Module2/pom.xml').and_return false

          result = PackageLibrary.maven_module?('/home/user/myproject/Module2')

          expect(result).to eq(false)
        end
      end
    end
  end
end

def mock_maven_module(path)
  allow(File).to receive(:exist?).with("#{path}/pom.xml").and_return true
end

def mock_git_module(path)
  allow(File).to receive(:exist?).with("#{path}/.git").and_return true
end
