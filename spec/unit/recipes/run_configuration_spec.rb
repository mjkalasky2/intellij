require 'spec_helper'
require_relative '../../../../intellij/libraries/run_configuration'

describe 'intellij_run_configuration' do
  describe 'testing the resource' do
    step_into :intellij_run_configuration
    platform 'ubuntu'

    context 'creating the run configuration' do
      recipe do
        intellij_run_configuration 'Start simple batch script' do
          project_path '/home/user/myproject'
          type 'BatchConfigurationType'
          factory 'Batch'
          open_tool_window true
          body <<-EOH
            <option name="INTERPRETER_OPTIONS" value="" />
            <option name="WORKING_DIRECTORY" value="$PROJECT_DIR$" />
            <option name="PARENT_ENVS" value="true" />
            <module name="" />
            <option name="SCRIPT_NAME" value="$PROJECT_DIR$/example.bat" />
            <option name="PARAMETERS" value="Hello" />
            <method v="2" />
          EOH
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      before do
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myproject/.idea/workspace.xml').and_return '<project version="4"></project>'
      end

      it { is_expected.to create_intellij_run_configuration('Start simple batch script') }
      it { is_expected.to create_directory('/home/user/myproject/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/user/myproject/.idea/workspace.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/workspace.xml')
          .with(target: '/project/component[@name="RunManager"]/configuration[@name="Start simple batch script"]')
      end
      it do
        is_expected.to_not replace_xml('/home/user/myproject/.idea/workspace.xml')
          .with(target: '/project/component[@name="RunManager"]')
      end
    end

    context 'creating the run configuration selecting it as a default one' do
      recipe do
        intellij_run_configuration 'Start simple batch script' do
          project_path '/home/user/myproject'
          type 'BatchConfigurationType'
          factory 'Batch'
          open_tool_window false
          selected true
          body <<-EOH
            <option name="INTERPRETER_OPTIONS" value="" />
            <option name="WORKING_DIRECTORY" value="$PROJECT_DIR$" />
            <option name="PARENT_ENVS" value="true" />
            <module name="" />
            <option name="SCRIPT_NAME" value="$PROJECT_DIR$/example.bat" />
            <option name="PARAMETERS" value="Hello" />
            <method v="2" />
          EOH
          user 'myuser'
          group 'mygroup'
          action :create
        end
      end

      before do
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myproject/.idea/workspace.xml').and_return '<project version="4"></project>'
      end

      it { is_expected.to create_intellij_run_configuration('Start simple batch script') }
      it { is_expected.to create_directory('/home/user/myproject/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/user/myproject/.idea/workspace.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/workspace.xml')
          .with(target: '/project/component[@name="RunManager"]/configuration[@name="Start simple batch script"]')
      end
      it do
        is_expected.to replace_xml('/home/user/myproject/.idea/workspace.xml')
          .with(target: '/project/component[@name="RunManager"]')
      end
    end

    context 'deleting the run configuration' do
      recipe do
        intellij_run_configuration 'Start simple batch script' do
          project_path '/home/user/myproject'
          factory 'Bash'
          action :delete
        end
      end

      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myproject/.idea/workspace.xml').and_return true
      end

      it { is_expected.to delete_intellij_run_configuration('Start simple batch script') }
      it do
        is_expected.to remove_xml('/home/user/myproject/.idea/workspace.xml')
          .with(target: '/project/component[@name="RunManager"]/configuration[@name="Start simple batch script"]')
      end
      it do
        is_expected.to replace_xml('/home/user/myproject/.idea/workspace.xml')
          .with(
            target: '/project/component[@name="RunManager"][@selected="Bash.Start simple batch script"]/@selected',
            fragment: '<selected></selected>'
          )
      end
    end
  end

  describe 'testing the run configuration library' do
    let(:config_name) { 'My name' }
    let(:type) { 'type' }
    let(:factory) { 'factory' }
    let(:body) { '<options/>' }

    before do
      class RunConfigLibrary
        include IntelliJ::Workspace::RunConfiguration
      end
    end

    context 'generating the xml for the run configuration' do
      it 'generates proper xml when no folder is specified' do
        attributes = {
          name: config_name,
          type: type,
          factory: factory,
          folderName: nil,
        }
        expected_xml = <<-EOH
           <configuration name="#{config_name}" type="#{type}" factory="#{factory}">
            #{body}
          </configuration>
        EOH
        result = RunConfigLibrary.generate_run_config_xml(attributes, body)

        expected_xml.gsub!(/\s+/, ' ')
        result.gsub!(/\s+/, ' ')

        expect(result).to eq(expected_xml)
      end

      it 'generates proper xml when a folder is specified' do
        attributes = {
          name: config_name,
          type: type,
          factory: factory,
          folderName: 'my folder',
        }
        expected_xml = <<-EOH
           <configuration name="#{config_name}" type="#{type}" factory="#{factory}" folderName="my folder">
            #{body}
          </configuration>
        EOH
        result = RunConfigLibrary.generate_run_config_xml(attributes, body)

        expected_xml.gsub!(/\s+/, ' ')
        result.gsub!(/\s+/, ' ')

        expect(result).to eq(expected_xml)
      end
    end
  end
end
