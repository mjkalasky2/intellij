require 'spec_helper'
require_relative '../../../../intellij/libraries/.helpers'
require_relative '../../../../intellij/libraries/.jetbrains.api.rb'
require_relative '../../../../intellij/libraries/package'

describe 'intellij_default' do
  describe 'testing the resource' do
    step_into :intellij
    platform 'ubuntu'

    context 'installing the intellij from the tar.gz file' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version '2018.3.4'
          action :update
          user 'myuser'
          group 'mygroup'
          action :install
          source 'http://source/intellij.tar.gz'
          properties(
            'idea.max.intellisense.filesize': 5300,
            'idea.config.path': '${user.home}/.IntelliJIdea/config1'
          )
        end
      end

      before do
        mock_intellij_not_exists('/home/myuser/IntelliJ')
      end

      it { is_expected.to install_intellij('/home/myuser/IntelliJ') }
      it { is_expected.to create_directory('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_remote_file("#{Dir.tmpdir}/intellij.tar.gz").with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to_not extract_zipfile("#{Dir.tmpdir}/intellij.tar.gz") }
      it { is_expected.to extract_local_tar_extract("#{Dir.tmpdir}/intellij.tar.gz").with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to merge_java_properties('/home/myuser/IntelliJ/bin/idea.properties') }
    end

    context 'installing the intellij from the zip file' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version '2018.3.4'
          action :update
          user 'myuser'
          group 'mygroup'
          action :install
          source 'http://source/intellij.zip'
        end
      end

      before do
        mock_intellij_not_exists('/home/myuser/IntelliJ')
      end

      it { is_expected.to install_intellij('/home/myuser/IntelliJ') }
      it { is_expected.to create_directory('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_remote_file("#{Dir.tmpdir}/intellij.zip").with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to extract_zipfile("#{Dir.tmpdir}/intellij.zip") }
      it { is_expected.to_not extract_local_tar_extract("#{Dir.tmpdir}/intellij.zip").with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to_not merge_java_properties('/home/myuser/IntelliJ/bin/idea.properties') }
    end

    context 'installing the intellij when it is already installed' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version '2018.3.4'
          action :update
          user 'myuser'
          group 'mygroup'
          action :install
          source 'http://source/intellij.tar.gz'
          properties(
            'idea.max.intellisense.filesize': 5300,
            'idea.config.path': '${user.home}/.IntelliJIdea/config1'
          )
        end
      end

      before do
        mock_intellij_exists('2018.3.4', 'IC', '/home/myuser/IntelliJ')
      end

      it { is_expected.to create_directory('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to write_log('The IntelliJ community edition in /home/myuser/IntelliJ is already installed in the 2018.3.4 version').with_level(:info) }
      it { is_expected.to_not create_remote_file("#{Dir.tmpdir}/intellij.tar.gz") }
      it { is_expected.to_not extract_zipfile("#{Dir.tmpdir}/intellij.tar.gz") }
      it { is_expected.to_not extract_local_tar_extract("#{Dir.tmpdir}/intellij.tar.gz") }
      it { is_expected.to merge_java_properties('/home/myuser/IntelliJ/bin/idea.properties') }
    end

    context 'installing the intellij when it is already installed in the different version' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version '2018.3.4'
          action :update
          user 'myuser'
          group 'mygroup'
          action :install
          source 'http://source/intellij.tar.gz'
          properties(
            'idea.max.intellisense.filesize': 5300,
            'idea.config.path': '${user.home}/.IntelliJIdea/config1'
          )
        end
      end

      before do
        mock_intellij_exists('2018.3.3', 'IU', '/home/myuser/IntelliJ')
      end

      it { is_expected.to create_directory('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to write_log('The IntelliJ in /home/myuser/IntelliJ is already installed, but does not match requirements (edition: IC => IU, version: 2018.3.4 => 2018.3.3)').with_level(:warn) }
      it { is_expected.to_not create_remote_file("#{Dir.tmpdir}/intellij.tar.gz") }
      it { is_expected.to_not extract_zipfile("#{Dir.tmpdir}/intellij.tar.gz") }
      it { is_expected.to_not extract_local_tar_extract("#{Dir.tmpdir}/intellij.tar.gz") }
      it { is_expected.to merge_java_properties('/home/myuser/IntelliJ/bin/idea.properties') }
    end

    context 'updating the intellij when there is no intellij installed' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version '2018.3.4'
          action :update
          user 'myuser'
          group 'mygroup'
          action :update
          source 'http://source/intellij.tar.gz'
          properties(
            'idea.max.intellisense.filesize': 5300,
            'idea.config.path': '${user.home}/.IntelliJIdea/config1'
          )
        end
      end

      before do
        mock_intellij_not_exists('/home/myuser/IntelliJ')
      end

      it { is_expected.to create_directory('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to delete_directory('/home/myuser/IntelliJ') }
      it { is_expected.to install_intellij('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
    end

    context 'updating the intellij when it is already installed' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version '2018.3.4'
          action :update
          user 'myuser'
          group 'mygroup'
          action :update
          source 'http://source/intellij.tar.gz'
          properties(
            'idea.max.intellisense.filesize': 5300,
            'idea.config.path': '${user.home}/.IntelliJIdea/config1'
          )
        end
      end

      before do
        mock_intellij_exists('2018.3.4', 'IC', '/home/myuser/IntelliJ')
      end

      it { is_expected.to write_log('The IntelliJ community edition in /home/myuser/IntelliJ is already installed in the 2018.3.4 version').with_level(:info) }
      it { is_expected.to_not delete_directory('/home/myuser/IntelliJ') }
      it { is_expected.to_not install_intellij('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
    end

    context 'updating the intellij when it is already installed in the different version' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version '2018.3.4'
          action :update
          user 'myuser'
          group 'mygroup'
          action :update
          source 'http://source/intellij.tar.gz'
          properties(
            'idea.max.intellisense.filesize': 5300,
            'idea.config.path': '${user.home}/.IntelliJIdea/config1'
          )
        end
      end

      before do
        mock_intellij_exists('2018.3.3', 'IU', '/home/myuser/IntelliJ', true)
      end

      it { is_expected.to create_directory('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to write_log('Removing the previous IntelliJ 2018.3.3 IU edition in /home/myuser/IntelliJ').with_level(:info) }
      it { is_expected.to delete_directory('/home/myuser/IntelliJ') }
      it { is_expected.to install_intellij('/home/myuser/IntelliJ').with(user: 'myuser', group: 'mygroup') }
    end

    context 'deleting the intellij' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          action :delete
        end
      end

      before do
        mock_intellij_exists('2018.3.3', 'IU', '/home/myuser/IntelliJ')
      end

      it { is_expected.to write_log('Removing the previous IntelliJ 2018.3.3 IU edition in /home/myuser/IntelliJ').with_level(:info) }
      it { is_expected.to delete_directory('/home/myuser/IntelliJ') }
    end

    context 'installing a non-existing version of the IntelliJ IDEA' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          version 'non-existing-version'
          action :update
        end
      end

      it { expect { subject }.to raise_exception IntelliJ::Exceptions::IntelliJVersionNotFound }
    end

    context 'using unsupported archive type' do
      recipe do
        intellij '/home/myuser/IntelliJ' do
          edition :community
          source 'https://abcd.com/non-supported.archive'
          action :install
        end
      end

      it { expect { subject }.to raise_exception IntelliJ::Exceptions::IntelliJArchiveTypeNotSupported }
    end
  end

  describe 'testing the package library' do
    before do
      class PackageLibrary
        include IntelliJ::Install
      end

      context 'checking if intellij exists' do
        it 'returns true when it does exist' do
          mock_intellij_exists('2018.3.4', 'IC', '/home/myuser/IntelliJ')

          result = PackageLibrary.intellij_installed?('/home/myuser/IntelliJ')

          expect(result).to eq(true)
        end

        it 'returns false when it does not exist' do
          mock_intellij_not_exists('/home/myuser/IntelliJ')

          result = PackageLibrary.intellij_installed?('/home/myuser/IntelliJ')

          expect(result).to eq(false)
        end
      end
    end
  end
end
