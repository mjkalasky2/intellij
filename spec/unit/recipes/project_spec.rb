require 'spec_helper'
require_relative '../../../../intellij/libraries/project'

describe 'intellij_project' do
  describe 'testing the resource' do
    step_into :intellij_project
    platform 'ubuntu'

    context 'creating the project' do
      recipe do
        intellij_project '/home/user/myproject' do
          project_name 'My fantastic project'
          default_sdk_name 'JDK 10.0'
          default_sdk_type :JavaSDK
          action :create
          user 'myuser'
          group 'mygroup'
        end
      end

      it { is_expected.to create_intellij_project('/home/user/myproject') }
      it { is_expected.to create_directory('/home/user/myproject').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_directory('/home/user/myproject/.idea').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_file('/home/user/myproject/.idea/.name').with(user: 'myuser', group: 'mygroup', content: 'My fantastic project') }
      it { is_expected.to create_if_missing_template('/home/user/myproject/.idea/misc.xml').with(user: 'myuser', group: 'mygroup') }
      it do
        is_expected.to append_if_missing_xml('/home/user/myproject/.idea/misc.xml')
          .with(target: '/project/component[@name="ProjectRootManager"]')
      end
    end

    context 'deleting the project' do
      recipe do
        intellij_project '/home/user/myproject' do
          project_name 'My fantastic project'
          action :delete
        end
      end

      it { is_expected.to delete_intellij_project('/home/user/myproject') }
      it { is_expected.to delete_directory('/home/user/myproject/.idea') }
    end
  end

  describe 'testing the run configuration library' do
    let(:language_level) { 'JDK_10' }

    before do
      class ProjectLibrary
        include IntelliJ::Project
      end
    end

    context 'generating the SDK fragment' do
      it 'returns default xml for no SDK' do
        expected_xml = <<-EOH
          <component name=\"ProjectRootManager\" version=\"2\" languageLevel=\"#{language_level}\" default=\"false\"/>
        EOH

        result = ProjectLibrary.get_default_sdk_fragment(nil, nil, language_level)

        result.gsub!(/\s+/, ' ')
        expected_xml.gsub!(/\s+/, ' ').gsub!(/^\s+/, '').gsub!(/\s+$/, '')

        expect(result).to eq(expected_xml)
      end

      it 'returns xml for the JavaSDK' do
        sdk_name = 'JDK 10'
        sdk_type = :JavaSDK
        expected_xml = <<-EOH
         <component name="ProjectRootManager" version="2" languageLevel="#{language_level}" default="false"
            project-jdk-name="#{sdk_name}" project-jdk-type="#{sdk_type}">
           <output url="file://$PROJECT_DIR$/out" />
         </component>
        EOH

        result = ProjectLibrary.get_default_sdk_fragment(sdk_name, sdk_type, language_level)

        result.gsub!(/\s+/, ' ')
        expected_xml.gsub!(/\s+/, ' ')

        expect(result).to eq(expected_xml)
      end

      it 'returns xml for other SDKs' do
        sdk_name = 'Ruby 2.6'
        sdk_type = :RUBY_SDK
        expected_xml = <<-EOH
          <component name=\"ProjectRootManager\"
              version=\"2\" languageLevel=\"#{language_level}\" default=\"false\"
              project-jdk-name=\"#{sdk_name}\" project-jdk-type=\"#{sdk_type}\" />
        EOH

        result = ProjectLibrary.get_default_sdk_fragment(sdk_name, sdk_type, language_level)

        result.gsub!(/\s+/, ' ')
        expected_xml.gsub!(/\s+/, ' ').gsub!(/^\s+/, '').gsub!(/\s+$/, '')

        expect(result).to eq(expected_xml)
      end
    end
  end
end
