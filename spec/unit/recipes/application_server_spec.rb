require 'spec_helper'

describe 'intellij_application_server' do
  describe 'testing the resource' do
    step_into :intellij_application_server
    platform 'ubuntu'

    context 'creating the server' do
      before do
        allow(File).to receive(:read).with(anything).and_call_original
        allow(File).to receive(:read).with('/home/user/myworkspace/config/options/remote-servers.xml').and_return '<application/>'
      end

      recipe do
        intellij_application_server 'Local Apache Tomcat' do
          type :Tomcat
          path '/home/user/tomcat'
          user 'myuser'
          group 'mygroup'
          workspace_path '/home/user/myworkspace'
          action :create
        end
      end

      it { is_expected.to create_intellij_application_server('Local Apache Tomcat') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/options').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_if_missing_file('/home/user/myworkspace/config/options/javaeeAppServers.xml').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to bulk_edit_xml('/home/user/myworkspace/config/options/javaeeAppServers.xml') }
    end

    context 'deleting the server' do
      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/options/javaeeAppServers.xml').and_return true
      end

      recipe do
        intellij_application_server 'Local Apache Tomcat' do
          type :Tomcat
          path '/home/user/tomcat'
          workspace_path '/home/user/myworkspace'
          action :delete
        end
      end

      it { is_expected.to delete_intellij_application_server('Local Apache Tomcat') }
      it { is_expected.to bulk_edit_xml('/home/user/myworkspace/config/options/javaeeAppServers.xml') }
    end
  end
end
