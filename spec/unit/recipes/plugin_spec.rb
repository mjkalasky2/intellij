require 'spec_helper'

require_relative '../../../../intellij/libraries/.jetbrains.api.rb'
require_relative '../../../../intellij/libraries/plugin'

describe 'intellij_plugin' do
  describe 'testing the resource' do
    step_into :intellij_plugin
    platform 'ubuntu'

    context 'installing the plugin from the IntelliJ repository without defining the IntelliJ path' do
      before do
        allow_any_instance_of(IntelliJ::Plugins).to receive(:plugin_data_from_repository).with('Docker', :community, nil, nil).and_return(
          'update' => 001,
          'source' => 'http://downloads/Docker.zip',
          'edition' => :community
        )
      end

      recipe do
        intellij_plugin 'Docker' do
          workspace '/home/user/myworkspace'
          edition :community
          user 'myuser'
          group 'mygroup'
          action :install
        end
      end

      it { is_expected.to install_intellij_plugin('Docker') }
      it { is_expected.to write_log('Installing the plugin Docker for IntelliJ community edition from IntelliJ\'s repository') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/plugins').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_remote_file('/home/user/myworkspace/config/plugins/Docker.zip').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to extract_zipfile('/home/user/myworkspace/config/plugins/Docker.zip') }
      it { is_expected.to delete_file('/home/user/myworkspace/config/plugins/Docker.zip') }
    end

    context 'installing the non-installed .jar plugin' do
      recipe do
        intellij_plugin 'My fancy plugin' do
          workspace '/home/user/myworkspace'
          edition :community
          user 'myuser'
          group 'mygroup'
          source 'http://downloads/plugin.jar'
          action :install
        end
      end

      it { is_expected.to install_intellij_plugin('My fancy plugin') }
      it { is_expected.to write_log('Installing the plugin My fancy plugin from specific source: http://downloads/plugin.jar') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/plugins').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_remote_file('/home/user/myworkspace/config/plugins/plugin.jar').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to_not extract_zipfile('/home/user/myworkspace/config/plugins/plugin.jar') }
      it { is_expected.to_not delete_file('/home/user/myworkspace/config/plugins/plugin.jar') }
    end

    context 'installing the non-installed .zip plugin' do
      recipe do
        intellij_plugin 'My fancy plugin' do
          workspace '/home/user/myworkspace'
          edition :community
          user 'myuser'
          group 'mygroup'
          source 'http://downloads/plugin.zip'
          action :install
        end
      end

      it { is_expected.to install_intellij_plugin('My fancy plugin') }
      it { is_expected.to write_log('Installing the plugin My fancy plugin from specific source: http://downloads/plugin.zip') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/plugins').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_remote_file('/home/user/myworkspace/config/plugins/plugin.zip').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to extract_zipfile('/home/user/myworkspace/config/plugins/plugin.zip') }
      it { is_expected.to delete_file('/home/user/myworkspace/config/plugins/plugin.zip') }
    end

    context 'installing the already installed plugin' do
      before do
        allow_any_instance_of(IntelliJ::Helpers).to receive(:zip_contents).with('/home/user/myworkspace/config/plugins/plugin.zip').and_return ['Plugin']
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/plugins/Plugin').and_return true
      end

      recipe do
        intellij_plugin 'My fancy plugin' do
          workspace '/home/user/myworkspace'
          edition :community
          user 'myuser'
          group 'mygroup'
          source 'http://downloads/plugin.zip'
          action :install
        end
      end

      it { is_expected.to install_intellij_plugin('My fancy plugin') }
      it { is_expected.to write_log('Installing the plugin My fancy plugin from specific source: http://downloads/plugin.zip') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/plugins').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_remote_file('/home/user/myworkspace/config/plugins/plugin.zip').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to_not extract_zipfile('/home/user/myworkspace/config/plugins/plugin.zip') }
      it { is_expected.to delete_file('/home/user/myworkspace/config/plugins/plugin.zip') }
    end

    context 'updating the already installed plugin' do
      before do
        allow_any_instance_of(IntelliJ::Helpers).to receive(:zip_contents).with('/home/user/myworkspace/config/plugins/plugin.zip').and_return ['Plugin']
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/plugins/Plugin').and_return true
      end

      recipe do
        intellij_plugin 'My fancy plugin' do
          workspace '/home/user/myworkspace'
          edition :community
          user 'myuser'
          group 'mygroup'
          source 'http://downloads/plugin.zip'
          action :update
        end
      end

      it { is_expected.to update_intellij_plugin('My fancy plugin') }
      it { is_expected.to write_log('Installing the plugin My fancy plugin from specific source: http://downloads/plugin.zip') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/plugins').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to create_remote_file('/home/user/myworkspace/config/plugins/plugin.zip').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to extract_zipfile('/home/user/myworkspace/config/plugins/plugin.zip') }
      it { is_expected.to delete_file('/home/user/myworkspace/config/plugins/plugin.zip') }
    end

    context 'installing the already installed plugin with setting own repository file name' do
      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/home/user/myworkspace/config/plugins/my_plugin').and_return true
      end

      recipe do
        intellij_plugin 'My fancy plugin' do
          workspace '/home/user/myworkspace'
          edition :community
          user 'myuser'
          group 'mygroup'
          source 'http://downloads/plugin.zip'
          repository_filename 'my_plugin'
          action :install
        end
      end

      it { is_expected.to install_intellij_plugin('My fancy plugin') }
      it { is_expected.to write_log('Installing the plugin My fancy plugin from specific source: http://downloads/plugin.zip') }
      it { is_expected.to create_directory('/home/user/myworkspace/config/plugins').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to_not create_remote_file('/home/user/myworkspace/config/plugins/plugin.zip').with(user: 'myuser', group: 'mygroup') }
      it { is_expected.to_not extract_zipfile('/home/user/myworkspace/config/plugins/plugin.zip') }
      it { is_expected.to_not delete_file('/home/user/myworkspace/config/plugins/plugin.zip') }
    end

    context 'installing a non-existing plugin' do
      recipe do
        intellij_plugin 'A non existing plugin' do
          workspace '/home/user/myworkspace'
          edition :community
          action :install
        end
      end

      it { expect { subject }.to raise_exception IntelliJ::Exceptions::IntelliJPluginNotFound }
    end

    context 'installing a plugin with the invalid update' do
      recipe do
        intellij_plugin 'Docker' do
          workspace '/home/user/myworkspace'
          update 1234567890123
          edition :community
          action :install
        end
      end

      it { expect { subject }.to raise_exception IntelliJ::Exceptions::IntelliJPluginVersionNotFound }
    end

    context 'installing a plugin for a missing IntelliJ instance' do
      before do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/some/not/existing/directory').and_return false
      end

      recipe do
        intellij_plugin 'Docker' do
          workspace '/home/user/myworkspace'
          intellij_path '/some/not/existing/directory'
          update 1234567890123
          edition :community
          action :install
        end
      end

      it { expect { subject }.to raise_exception IntelliJ::Exceptions::IntelliJNotFound }
    end
  end
end
