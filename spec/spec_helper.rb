require 'chefspec'
require 'chefspec/berkshelf'
require 'rspec/expectations'

RSpec.configure do |config|
  config.color = true
  config.formatter = :documentation
  config.log_level = :info
  config.platform = 'ubuntu'
  config.run_all_when_everything_filtered = true
end

at_exit { ChefSpec::Coverage.report! }

def mock_intellij_exists(version, edition, path, only_first = false)
  second_call = !only_first
  allow(File).to receive(:exist?).with(anything).and_call_original
  allow(File).to receive(:exist?).with("#{path}/product-info.json").and_return true, second_call
  allow(File).to receive(:exist?).with("#{path}/build.txt").and_return true, second_call
  allow(File).to receive(:read).with(anything).and_call_original
  allow(File).to receive(:read).with("#{path}/product-info.json").and_return "{\"version\": \"#{version}\", \"productCode\": \"#{edition}\"}"
end

def mock_intellij_not_exists(path)
  allow(File).to receive(:exist?).with(anything).and_call_original
  allow(File).to receive(:exist?).with("#{path}/product-info.json").and_return false
  allow(File).to receive(:exist?).with("#{path}/build.txt").and_return false
end
