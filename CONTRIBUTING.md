# Contributing

If you're willing to contribute to this cookbook, please visit general rules of contributing for Chef cookbooks:

https://github.com/chef-cookbooks/community_cookbook_documentation/blob/master/CONTRIBUTING.MD