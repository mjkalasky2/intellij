# intellij

This cookbook allows installing and configuring the [IntelliJ IDEA IDE](https://www.jetbrains.com/idea/). It can 
install/update the IntelliJ package, configure "workspace" (the configuration from the `idea.config.path` directory),
install plugins from the Plugins Repository and configure projects (artifacts, run configurations, SDKs, modules etc.).

The main purpose of this cookbook is to be used as an automatic environment configuration for developers. 

Every project has its own configuration which should be added to IDE in order to make it work.
Configuring the IntelliJ IDEA can take some time (there are a lot of places which you can adjust to your needs!)
and - what is worse - can be forgotten, which leads to having a hard time when there is a need to create a new environment 
(for example, a new person comes to the project).
 
Storing your project's configuration inside Chef scripts is good not only because it can configure 
the whole IJ configuration for you - it can also be used as documentation for any new person.

## NOTICE: This cookbook is NOT idempotent!

One of the Chef's ideology is to [create idempotent cookbooks](https://github.com/clintoncwolfe/xml_edit#why-you-should-seriously-not-use-this)
that will always configure the environment the same way. This is NOT the case for this cookbook.

IntelliJ is a great tool with many configurations and developers are usually customizing their environments. It means that
Chef should not update all of the configuration, which would lead to losing any changes made by the developer, but instead,
it should only make sure that the base configuration stays the same. This way you can always repair your environment by
running the cookbook again - it will only restore the exact configuration written in the recipe and leave personal changes.

## Platforms

The cookbook supports Windows and most of the Linux platforms. 

## Usage

The main way of using the cookbook is to use its LWRP resources. 

### Recipes

#### Default.rb

The [default recipe](recipes/default.rb) will only install the IntelliJ IDEA without configuring anything. 
It uses the following attributes:

| Attribute                          | Default value                            | Description                                                                                                                              |
| ---------------------------------- | ---------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| ['intellij']['recipe']['path']     | C:/Program Files/JetBrains/IntelliJ      | Path where the IntelliJ will be installed. If the path is not empty, the IntelliJ will be updated to a specific version/edition.         |
| ['intellij']['recipe']['source']   | nil                                      | Source from which the cookbook will download the IntelliJ package. If empty, Chef will download it from the JetBrains site.              |
| ['intellij']['recipe']['checksum'] | nil                                      | Checksum for the IntelliJ package. If you did not set the source parameter, Chef will automatically download it from the JetBrains site. |
| ['intellij']['recipe']['edition']  | :community                               | Edition of the IntelliJ to be downloaded. Can be `:community` or `:ultimate`.                                                            |
| ['intellij']['recipe']['version']  | nil                                      | Version of the IntelliJ to be downloaded. If empty, Chef will automatically use the latest version.                                      |
| ['intellij']['recipe']['user']     | nil                                      | The user used to install the IntelliJ package.                                                                                           |
| ['intellij']['recipe']['group']    | nil                                      | The group used to install the IntelliJ package.                                                                                          |

### Resources

The main functionality of the cookbook lies within its resources. You can use them by defining dependency for 
the cookbook inside the `metadata.rb` file:

```ruby
depends 'intellij'
```

List of currently supported resources:
1. [intellij](#default) - managing the IntelliJ installation
1. [intellij_workspace](#workspace) - managing the IntelliJ global configuration
1. [intellij_plugin](#plugin) - managing plugins
1. [intellij_sdk](#sdk) - managing installed SDKs
1. [intellij_project](#project) - managing the IntelliJ's project
1. [intellij_module](#module) - managing modules inside the IntelliJ's project
1. [intellij_run_configuration](#run-configuration) - managing the IntelliJ's run configurations used in the project
1. [intellij_artifact](#artifact) - managing artifacts for IntelliJ's project
1. [intellij_changelist](#changelist) - managing change lists inside the IntelliJ's project
1. [intellij_docker_integration](#docker-integration) - managing the Docker configuration inside the IntelliJ workspace
1. [intellij_application_server](#application-server) - managing the Java EE servers installed in the IntelliJ workspace
1. [intellij_shortcut](#shortcut) - managing the shortcuts for the IntelliJ instance

#### Default

[Default resource](resources/default.rb) `intellij` is used to download/update the IntelliJ installation. 

| Action   | Description                                                                                                                                                                                     |
| -------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| install  | This action will install new IntelliJ package. If the path is used by the different installation, it will compare them and display a warning if the versions are not the same.  Default action. |
| delete   | This action will remove the IntelliJ package from the specified path.                                                                                                                           |
| update   | This action is mainly the same as the `install` action, but will update the existing installation rather than just displaying a warning message.                                                |

| Property       | Type   | Description                                                                                                                                                                                                                            |
| -------------- | ------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| path           | String | Path where the IntelliJ package will be installed. Defined also as name attribute.                                                                                                                                                     |
| version        | String | Version of the IntelliJ to be downloaded. If empty, Chef will automatically use the latest version.                                                                                                                                    |
| source         | String | Source from which the cookbook will download the IntelliJ package. If empty, Chef will download it from the JetBrains site. If you are specifying your own source, please set also the `checksum`, `version` and `edition` parameters. |
| checksum       | String | Checksum for the IntelliJ package. If you did not set the source parameter, Chef will automatically download it from the JetBrains site.                                                                                               |
| edition        | Symbol | Edition of the IntelliJ to be downloaded. Can be `:community` or `:ultimate`.                                                                                                                                                          |
| properties     | Hash   | Hash with properties for the `idea.properties` file.                                                                                                                                                                                   |
| user           | String | The user used to install the IntelliJ package.                                                                                                                                                                                         |
| group          | String | The group used to install the IntelliJ package.                                                                                                                                                                                        |

Example:

```ruby
intellij 'Installing the 2018.3.4 IntelliJ from the JetBrains download site' do
  path '/home/myuser/IntelliJ'
  version '2018.3.4'
  user 'myuser'
  group 'mygroup'
  edition :community
  properties(
    'idea.config.path': '/home/myuser/workspace',
    'idea.fatal.error.notification': 'enabled'
  )
  action :update
end

intellij 'This will use source defined as property. Version and edition are useful.' do
  path '/home/myuser/IntelliJ'
  version '2018.3.4'
  source 'https://download.jetbrains.com/idea/ideaIU-2018.3.4.win.zip'
  checksum 'bff5d4f07762f2582db80240cfe1e6586cf47c304a7a2c50eb672e63be98229a'
  user 'myuser'
  group 'mygroup'
  edition :community
  action :update
end
```

#### Workspace

As a "workspace" we understand the main IntelliJ configuration such as plugins, installed SDKs or applications servers.
Unfortunately, the resource can not guess, where the default (global) configuration lies as the user can have multiple
instances of the IntelliJ installed. Because of this, the resource requires to define the path, where the workspace is
(usually in the home dir or defined inside the `idea.config.path` parameter).

Normally there is only one workspace defined for all IntelliJ projects. This is good when they have a common configuration,
the IntelliJ also does very good work allowing the user to overwrite it on the project level. 

Sometimes it is not enough and there is a need to define separated configuration for some projects - especially if you
work in a company with multiple projects which can have conflicts. There is a way to do it - and this cookbook does support it.
Every time you are creating a configuration using the `intellij_workspace` resource, it creates a `idea.properties` file
inside the `config` directory in the workspace. You can use it to create a special shortcut, which will start the IntelliJ
with proper configuration using the [intellij_shortcut](#shortcut) resource.

[Resource's configuration](resources/workspace.rb):

| Action   | Description                                                                                                                                                                  |
| -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| create   | This action will prepare new workspace for the IntelliJ and update all configuration that's had changed. Default action.                                                     |
| delete   | This action will remove the IntelliJ's workspace.                                                                                                                            |

| Property             | Type   | Description                                                                                                                                                               |
| -------------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| path                 | String | Path where the workspace will be created.                                                                                                                                 |
| intellij_path        | String | Path where the IntelliJ package is installed. It is used to create the shortcut to the workspace.                                                                         |
| default_project      | String | If set, the cookbook will set the project from this path as the default one (meaning that the IntelliJ will not ask you to open project location during the first usage). |
| color_scheme         | Symbol | Color scheme to be used. Can be `:IntelliJ` or `:Darcula`.                                                                                                                |
| code_style           | String | Source of the code style template in .xml format. This code style will be default for all projects in the workspace.                                                      |
| code_style_variables | Hash   | Optional variables for the code_style template.                                                                                                                           |
| user                 | String | The user used to create the workspace.                                                                                                                                    |
| group                | String | The group used to create the workspace.                                                                                                                                   |

Example:

```ruby
intellij_workspace 'Creating the separated Intellij configuration' do
  path '/home/myuser/workspace'
  default_project '/home/myuser/workspace/project'
  intellij_path '/home/myuser/IntelliJ'
  color_scheme :IntelliJ
  code_style 'styles/Default.xml'
  user 'myuser'
  group 'mygroup'
  action :create
end

intellij_workspace 'This will configure the global instance of configuration' do
  path '/home/myuser/.IdeaIC14'
  default_project '/home/myuser/workspace/project'
  intellij_path '/home/myuser/IntelliJ'
  color_scheme :Darcula
  user 'myuser'
  group 'mygroup'
  action :create
end
```

#### Plugin

[This resource](resources/plugin.rb) can be used to install the plugin from the [Jetbrains plugins repository](https://plugins.jetbrains.com/).
It will use the JetBrains API to find plugins matching the specified name. It also allows downloading the plugin from 
the specific update ("version"). If the plugin is downloaded as the .zip file, it will also automatically unzip it in the
proper location, so the user will not have to do it manually.

| Action   | Description                                                                                                                                                      |
| -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| install  | This action will download the plugin and then install it inside the proper directory. Default action. It will not update the plugin if it is already installed.  |
| update   | Same as the `:install` action but will always update the plugin (even if it is already installed).                                                               |

| Property               | Type     | Description                                                                                                                                                                                                                                                                  |
| ---------------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| name                   | String   | Name of the plugin. Should be defined as the resource's name. Has to match *exactly* the name displayed inside the plugins repository, otherwise Chef will raise an error.                                                                                                   |
| workspace              | String   | Path for the IntelliJ's workspace. It is used to define the proper path for the plugins installations. Can be also set up as the "global workspace" path.                                                                                                                    |
| source                 | String   | If set, the cookbook download the plugins from the different source than the official repository.                                                                                                                                                                            |
| update                 | Integer  | Version of the plugin to be downloaded. If empty, the cookbook will download the latest version.                                                                                                                                                                             |
| edition                | Symbol   | Some plugins are available only for one edition. To make sure that the correct plugin will be installed, you should pass the edition parameter. Can be `:community` or `:ultimate`.                                                                                          |
| repository_filename    | String   | .zip plugins not always have the same file name after extracting as the zip file. To avoid downloading the plugin every time, you can set this property and the resource will check if the plugin is already installed.                                                      |
| user                   | String   | The user used to install the plugin.                                                                                                                                                                                                                                         |
| group                  | String   | The group used to install the plugin.                                                                                                                                                                                                                                        |
| intellij_path          | String   | The path where the IntelliJ is installed. If this setting is provided, the resource will automatically detect the build number and edition of the IntelliJ and will try to install the most relevant plugin (will check, which version of the plugin supports the IntelliJ). |

Example:

```ruby
intellij_plugin 'Chef integration' do
  workspace '/home/myuser/.IdeaIC14'
  update 57884
  edition :ultimate
  action :install
end

intellij_plugin 'Docker' do
  workspace '/home/myuser/.IdeaIC14'
  respository_filename 'Docker'
  edition :community
  action :install
end

intellij_plugin 'Dart' do
  workspace '/home/myuser/.IdeaIC14'
  user 'myuser'
  group 'mygroup'
  source 'https://downloads/Dart.zip'
  action :update
end

intellij_plugin 'Docker' do
  workspace '/home/myuser/.IdeaIC14e'
  intellij_path '/home/myuser/JetBrains/IntelliJ'
  action :install
end
```

#### SDK

[This resource](resources/sdk.rb) can be used to install the SDK for all projects using the workspace.
Please, note that not all SDK types are available in the community edition of the IntelliJ.

| Action    | Description                                          |
| --------- | ---------------------------------------------------- |
| add       | This action configures the SDK inside the workspace. |
| delete    | This action will delete the SDK from the workspace.  |

| Property     | Type     | Description                                                                                                                                                                     |
| ------------ | -------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sdk_name     | String   | Name of the SDK. Should be defined as the resource's name.                                                                                                                      |
| workspace    | String   | Path for the IntelliJ's workspace.                                                                                                                                              |
| path         | String   | Path where the SDK is installed.                                                                                                                                                |
| type         | Symbol   | Type of the SDK. Can be `:JavaSDK` or `:RUBY_SDK.` More SDK's will be added if needed.                                                                                          |
| roots_config | String   | The XML contents of the SDK configuration from the IntelliJ. Since there are a lot of SDK types and they differ between each version, the resource provides a way to set it up. |
| user         | String   | The user used to install the IntelliJ's SDK.                                                                                                                                    |
| group        | String   | The group used to install the IntelliJ's SDK.                                                                                                                                   |

Example:

```ruby
intellij_sdk 'Ruby 2.6.1' do
  workspace  '/home/myuser/.IdeaIC14'
  path '/home/myuser/Ruby26-x64'
  type :RUBY_SDK
  action :add
end

intellij_sdk 'Java Open JDK 1.8' do
  workspace  '/home/myuser/.IdeaIC14'
  path '/path/to/java'
  type :JavaSDK
  roots_config <<-EOH
    <roots>
       ... more configuration
    </roots>
  EOH
  action :add
end
```

#### Project

[This resource](resources/project.rb) will create a new IntelliJ IDEA's project. 

| Action    | Description                                          |
| --------- | ---------------------------------------------------- |
| create    | This action will create and configure the project.   |
| delete    | This action will delete the project.                 |

| Property             | Type     | Description                                                                            |
| -------------------- | -------- | -------------------------------------------------------------------------------------- |
| path                 | String   | Path where the project will be created. Name attribute.                                |
| project_name         | String   | Optional project's name. If empty, the name of the directory will be used.             |
| default_sdk_name     | String   | Optional. Name of the SDK that will be set up as the default one.                      |
| default_sdk_type     | Symbol   | Type of the SDK. Can be `:JavaSDK` or `:RUBY_SDK`. More SDK's will be added if needed. |
| language_level       | String   | Language level of Java used within the project. Default: JDK_10.                       |
| code_style           | String   | Source of the code style template in .xml format.                                      |
| code_style_variables | Hash     | Optional variables for the code_style template.                                        |
| user                 | String   | The user used to create the project.                                                   |
| group                | String   | The group used to create the project.                                                  |

Example:

```ruby
intellij_project '/home/myuser/workspace/project' do
  project_name 'My fantastic project'
  default_sdk_name 'Ruby 2.6.1'
  default_sdk_type :RUBY_SDK
  code_style 'styles/Project.xml'
  action :create
end
```

#### Module

[This resource](resources/project.rb) will create a new module to the existing IntelliJ IDEA's project.
Modules are usually root directories of source codes for your applications. The resource assumes that you already have
them on the disk (either downloaded from the repository or simply created before using this cookbook.).

The resource will automatically detect whether the project is a Maven project. If yes, it will add it to the IJ's 
Maven configuration. 

It also tries to automatically detect which control version system is used (if any). Currently only
Git repositories will be detected - support for CVS and other ones will be added if needed.

| Action    | Description                                          |
| --------- | ---------------------------------------------------- |
| create    | This action will create and configure the module.    |
| delete    | This action will delete the module.                  |

| Property         | Type     | Description                                                                                                                                                                                        |
| ---------------- | -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| path             | String   | Path where the module is created.                                                                                                                                                                  |
| type             | Symbol   | Type of the module. IntelliJ has different types for different languages/frameworks. Currently, possible options are: `:JAVA_MODULE`, `:RUBY_MODULE` and `:WEB_MODULE.` Defaults to `:WEB_MODULE`. |
| sdk_type         | Symbol   | Optional SDK type for this specific module. Can be `:JavaSDK`, `:RUBY_SDK` or `:none`.                                                                                                             |
| sdk_name         | String   | Optional SDK name for this specific module.                                                                                                                                                        |
| module_name      | String   | Optional module's name. If not set, the name of the directory will be used.                                                                                                                        |
| project_path     | String   | Path where the project is created. Required.                                                                                                                                                       |
| config_directory | String   | Path where the module's configuration (the `.iml` file) will be created. If not set, module's path will be used.                                                                                   |
| user             | String   | The user used to install the module.                                                                                                                                                               |
| group            | String   | The group used to install the module.                                                                                                                                                              |

Example:

```ruby
intellij_module '/home/myuser/Maven-module' do
  project_path '/home/myuser/workspace/project'
  type :JAVA_MODULE
  module_name 'sample-maven-module'
  sdk_name 'JDK 10.0'
  sdk_type :JavaSDK
  action :create
end
```

#### Run configuration

[This resource](resources/run_configuration.rb) can be used to define Run configurations used within the project.

Run configurations are usually stored within the `.idea/workspace.xml` file, inside the `RunManager` component. 
There are many plugins which are adding new possible templates for RC, so it is almost impossible to cover 
all of the types in this cookbook. Instead of making a resource for every type, you should provide the configuration as XML.
When using the resource, please make sure that the configuration will be taken from the `workspace.xml` file. 

| Action    | Description                                                     |
| --------- | --------------------------------------------------------------- |
| create    | This action will create and configure the Run Configuration.    |
| delete    | This action will delete the Run Configuration.                  |

| Property         | Type     | Description                                                                                                                                                      |
| ---------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| config_name      | String   | Name of the Run configuration. Should be defined as the resource's name.                                                                                         |
| type             | String   | Type for the Run configuration. Please check the `workspace.xml` file to get this value.                                                                         |
| factory          | String   | Factory for the Run configuration. Please check the `workspace.xml` file to get this value.                                                                      |
| folder           | String   | Optional. If set, the resource will move this configuration inside this folder.                                                                                  |
| body             | String   | Body of the run configuration. Please check the `workspace.xml` file to get this value. You should copy the XML fragment INSIDE the <configuration> element.     |
| project_path     | String   | Path where the project is created. Required.                                                                                                                     |
| open_tool_window | Boolean  | If set to true, the IntelliJ IDEA will automatically open the tool window before running the action. Default to `true`.                                          |
| selected         | Boolean  | If set to true, the the run configuration will be set as a default one (will appear automatically as the last selected). Default: `false`.                       |
| attributes       | Hash     | Map with additional attributes for thee run configuration. Some plugins have non-standard attributes (like Docker, for example).                                 |
| user             | String   | The user used to install the run configuration.                                                                                                                  |
| group            | String   | The group used to install the run configuration.                                                                                                                 |

Example:

```ruby
intellij_run_configuration 'Start simple batch script' do
  project_path '/home/myuser/workspace/project'
  type 'BatchConfigurationType'
  factory 'Batch'
  open_tool_window false
  body <<-EOH
    <option name="INTERPRETER_OPTIONS" value="" />
    <option name="WORKING_DIRECTORY" value="$PROJECT_DIR$" />
    <option name="PARENT_ENVS" value="true" />
    <module name="" />
    <option name="SCRIPT_NAME" value="$PROJECT_DIR$/example.bat" />
    <option name="PARAMETERS" value="Hello" />
    <method v="2" />
  EOH
  action :create
end

intellij_run_configuration 'Create Docker run configuration' do
  project_path '/home/myuser/workspace/project'
  type 'docker-deploy'
  factory 'dockerfile'
  attributes(
    'server-name': 'Local Docker'
  )
  selected true
  body <<-EOH
    <deployment type="docker-compose.yml">
      <settings>
        <option name="sourceFilePath" value="docker/docker-compose.yml" />
      </settings>
    </deployment>
    <method v="2"/>
  EOH
  action :create
end
```

#### Artifact

[This resource](resources/artifact.rb) can be used to add an IntelliJ's artifact inside the project.
Similar to the *Run configuration* resource, it uses whole XML fragments to avoid making very complicated logic for all
possible types of artifacts.

| Action    | Description                                            |
| --------- | ------------------------------------------------------ |
| create    | This action will create and configure the artifact.    |
| delete    | This action will delete the artifact.                  |

| Property     | Type     | Description                                                                                                                     |
| ------------- | -------- | ------------------------------------------------------------------------------------------------------------------------------ |
| artifact_name | String   | Name of the artifact. Resource's name attribute.                                                                               |
| output_path   | String   | Path where the artifact will be created after rebuilding sources within IntelliJ.                                              |
| root          | String   | <root> element of the artifact. Please check the original artifact file (in `./idea/artifacts` directory) to get this value.   |
| project_path  | String   | Path where the project is created. Required.                                                                                   |
| type          | String   | The type of the artifact. Please check the original artifact file (in `./idea/artifacts` directory) to get this value.         |
| user          | String   | The user used to install the artifact.                                                                                         |
| group         | String   | The group used to install the artifact.                                                                                        |

Example:

```ruby
intellij_artifact 'sample-maven-module:jar' do
  project_path 'C:/Test/Workspace/My Project'
  type 'jar'
  root <<-EOH
    <root id="archive" name="sample-maven-module.jar">
      <element id="module-output" name="sample-maven-module" />
    </root>
  EOH
  action :create
end
```

#### Changelist

[This resource](resources/changelist.rb) allows configuring the change lists within the IntelliJ. 
Change lists are useful because they allow the user to distinguish the changes made in code into the smaller groups.
Another great way to use them is to mark changes as "not pushable" (sometimes project requires to change the files
without committing them - this way you will not push them by mistake).

Please note that if the IntelliJ contains only one change list, it will be automatically set up as the default one,
meaning that any future edits will be saved in it. If you still want to have the `Default` change list, you have to
set it up using the same resource.

| Action       | Description                                                                                                                                                                                                                                                   |
| ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| create       | The change list will be set as exactly as defined with this resource. Any existing change list will be completely replaced.                                                                                                                                   |
| delete       | The change list, if existing, will be removed.                                                                                                                                                                                                                |
| update       | If the change list does not exist, it creates it with parameters defined within the resource. If it does exist, only provided parameters will be updated (not replacing unspecified attributes).                                                              |
| add_files    | If the change list exists, the resource will update the provided parameters. If the change list had files other than provided ones, the latter will be added to change list (not removing existing files). If the change list does not exist, it will add it. |
| delete_files | If the change list exists, the resource will update the provided parameters. If the user specified files, the resource will remove them from the existing change list. If the change list did not exist, it will create it with empty files list.             |

| Property        | Type          | Description                                                                                                                    |
| --------------- | ------------- | ------------------------------------------------------------------------------------------------------------------------------ |
| changelist_name | String        | Name of the change list. Resource's name attribute.                                                                            |
| comment         | String        | Optional description for the change list.                                                                                      |
| default         | Boolean       | Sets the resource as default if `true`.                                                                                        |
| files           | String, Array | The list of files to be included within the change list. The resource will remove those files from different change lists.     |
| project_path    | String        | Path where the project is created. Required.                                                                                   |
| user            | String        | The user used to create the workspace file.                                                                                    |
| group           | String        | The group used to create the workspace file.                                                                                   |

Examples:

```ruby
intellij_changelist 'This will make sure that the change list will be defined exactly as specified' do
  project_path '/home/user/my/project'
  changelist_name 'My changelist'
  comment 'Testing the change lists mechanism'
  files %w(/home/user/my/project/file1.txt /home/user/my/project/file2.txt)
  action :create
end

intellij_changelist 'This will only update the comment and set up the change list as default' do
  project_path '/home/user/my/project'
  changelist_name 'My changelist'
  comment 'Changed the comment'
  default true
  action :update
end

intellij_changelist 'This will only update the files within the change list exactly as provided' do
  project_path '/home/user/my/project'
  changelist_name 'My changelist'
  files %w(/home/user/my/project/new-file.txt)
  action :update
end

intellij_changelist 'This will update the change list and add files instead of replacing them' do
  project_path '/home/user/my/project'
  changelist_name 'My changelist'
  comment 'Changed the comment again'
  files %w(/home/user/my/project/added-file.txt)
  action :add_files
end

intellij_changelist 'This will update the change list and remove the file from it' do
  project_path '/home/user/my/project'
  changelist_name 'My changelist'
  default false
  files %w(/home/user/my/project/new-file.txt)
  action :delete_files
end

intellij_changelist 'This will completely remove the change list' do
  project_path node['intellij']['tests']['project_path']
  changelist_name 'My changelist'
  action delete
end

```

#### Docker integration

[This resource](resources/docker_integration.rb) can be used to configure the integration with Docker within the IntelliJ.
It can automatically install the plugin, configure the Docker connection (remembering about proper settings like sockets
addresses) and add Docker registries.

Please make sure that the [Docker integration](https://plugins.jetbrains.com/plugin/7724-docker-integration) plugin is
correctly installed in the IntelliJ's workspace.

| Action       | Description                                                                                                                                                                                                                                           |
| ------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| create       | Creates the configuration if not already exists, otherwise will replace existing configuration.                                                                                           |
| merge        | Creates the configuration if not already exists, otherwise will merge existing configuration (instead of replacing shared directories and registries, the resource will just merge them). |
| delete       | Removes provided Docker integration from the IntelliJ.                                                                                                                                    |

| Property           | Type          | Description                                                                                                                               |
| ------------------ | ------------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| server_name        | String        | Name of the Docker connection (remote server). Resource's name attribute.                                                                 |
| workspace_path     | String        | Required path to the workspace.                                                                                                           |
| machine_name       | String        | Provide this setting only if you are using the Docker machine. If passed, it will overwrite the connection (instead of using the socket). |
| api_url            | String        | Address for the Docker socket. If not provided, the resource will try to set it up according to your system.                              |
| registries         | Hash          | Hash with registries, where the key is the name of the registry and value is hash with keys: address (required), email and username.      |
| shared_directories | Hash          | Hash with path mappings, where the key is the local path and the value is the path inside the Docker container/machine.                   |                                        |
| user               | String        | The user used to create the Docker integration configuration.                                                                             |
| group              | String        | The group used to create the Docker integration configuration.                                                                            |

Examples:

```ruby
intellij_docker_integration 'Local Docker' do
  workspace_path '/home/myuser/workspace'
  registries(
    'Docker Registry': {
      address: 'registry.hub.docker.com',
      username: 'myuser',
      email: 'my@gmail.com',
    }
  )
  shared_directories(
    '/home/local/path': '/path/inside/docker'
  )
  user 'myuser'
  group 'mygroup'
end

intellij_docker_integration 'Local Docker' do
  workspace_path '/home/myuser/workspace'
  machine_name 'local_machine'
  registries(
    'My private registry': {
      address: 'my.company.registry.address',
      email: 'my@mycompany.com',
    }
  )
  shared_directories(
    '/home/local/path': '/changes/path/in/docker',
    '/home/local/path2': '/path/inside/docker'
  )
  action :merge
end
```

#### Application server

[This resource](resources/application_server.rb) can be used to add an instance of a Java EE server to the IntelliJ's workspace.
Please remember, that this functionality works only in the Ultimate version of the IntelliJ.

| Action       | Description                                                                                                                                                                                                                                           |
| ------------ | ---------------------------------------------------------- |
| create       | Default action. Creates a new instance of the server.      |
| delete       | Removes existing instance from the IntelliJ's workspace.   |

| Property           | Type          | Description                                                                                                            |
| ------------------ | ------------- | ---------------------------------------------------------------------------------------------------------------------- |
| server_name        | String        | Name of the application server. Resource's name attribute.                                                             |
| workspace_path     | String        | Required path to the workspace.                                                                                        |
| path               | String        | Path to the local server instance.                                                                                     |
| type               | Symbol        | Type of the application server. Can be either `JBoss` or `Tomcat`. Support for more servers will be added if needed.   |
| user               | String        | The user used to create the application servers configuration.                                                         |
| group              | String        | The group used to create the application servers configuration.                                                        |

Examples:

```ruby
intellij_application_server 'Local Apache Tomcat' do
  type :Tomcat
  path node['intellij']['example']['tomcat_path']
  user user
  group group
  workspace_path 'C:/Tests/Workspace'
  action :create
end

intellij_application_server 'Local WildFly' do
  workspace_path 'C:/Tests/Workspace'
  action :delete
end
```

#### Shortcut

[This resource](resources/shortcut.rb) can be used to create the shortcut for the IntelliJ IDEA instance.
It also supports overriding the main configuration by defining the `idea_properties` property (by changing 
[the IDEA_PROPERTIES environment variable](https://intellij-support.jetbrains.com/hc/en-us/articles/207240985-Changing-IDE-default-directories-used-for-config-plugins-and-caches-storage)).

| Action       | Description                                                                                                                                                                                                                                           |
| ------------ | ---------------------------------------------------------- |
| create       | Default action. Creates a new shortcut for the IntelliJ.   |
| delete       | Removes existing shortcut.                                 |

| Property        | Type          | Description                                                                                                            |
| --------------- | ------------- | ---------------------------------------------------------------------------------------------------------------------- |
| intellij_path   | String        | Path to the IntelliJ IDEA instance. Resource's name attribute.                                                         |
| directory       | String        | Directory where the shortcut will be created. Default to user's desktop.                                               |
| idea_properties | String        | Define this if you need to have separated workspace configuration for the project.                                     |
| shortcut_name   | String        | The name of the shortcut. Defaults to "IntelliJ IDEA EDITION VERSION".                                                 |
| user            | String        | The user used to create the shortcut.                                                                                  |
| group           | String        | The group used to create the shortcut.                                                                                 |

Examples:

```ruby
intellij_shortcut 'C:/Program Files/JetBrains/IntelliJ'

intellij_shortcut 'This will create the shortcut in the different directory and name, using separated configuration' do
  intellij_path 'C:/Program Files/JetBrains/IntelliJ'
  shortcut_name 'My shortcut'
  idea_properties 'C:/Workspace/config/idea.properties'
  directory 'C:/Shortcuts/'
  action :create
end

intellij_shortcut 'This will create a desktop shortcut using global configuration' do
  intellij_path '/home/user/JetBrains/IntelliJ'
  shortcut_name 'My shortcut'
  action :create
end
```

### More examples
You can find more examples of usage for every resource inside the [recipes](recipes) directory.