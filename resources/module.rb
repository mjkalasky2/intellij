resource_name :intellij_module
default_action :create

property :path, String, name_property: true
property :type, Symbol, equal_to: [:JAVA_MODULE, :RUBY_MODULE, :WEB_MODULE], default: :WEB_MODULE
property :sdk_type, Symbol, equal_to: [:JavaSDK, :RUBY_SDK, :none], default: :none
property :sdk_name, String
property :module_name, String
property :project_path, String, required: true
property :config_directory, String
property :user, String
property :group, String

action :create do
  args = @new_resource

  module_file, content_location = generate_module_file_path(args.path, args.module_name, args.config_directory)

  directory ::File.dirname(module_file) do
    recursive true
    user args.user
    group args.group
  end

  template module_file do
    source 'module/main.iml.erb'
    cookbook 'intellij'
    user args.user
    group args.group
    variables(
      IS_MAVEN: maven_module?(args.path),
      SDK_NAME: args.sdk_name,
      SDK_TYPE: args.sdk_type,
      TYPE: args.type,
      CONTENT: content_location
    )
    action :create_if_missing
  end

  directory "#{args.project_path}/.idea" do
    recursive true
    user args.user
    group args.group
  end

  modules_file = "#{args.project_path}/.idea/modules.xml"
  relative_modules_file = project_relative_path(module_file, args.project_path)

  template modules_file do
    source 'project/modules.xml'
    cookbook 'intellij'
    user args.user
    group args.group
    action :create_if_missing
  end

  xml_edit modules_file do
    parent '/project/component[@name="ProjectModuleManager"]/modules'
    target "/project/component[@name=\"ProjectModuleManager\"]/modules/module[@filepath=\"#{relative_modules_file}\"]"
    fragment "<module fileurl=\"file://#{relative_modules_file}\" filepath=\"#{relative_modules_file}\" />"
    action :append_if_missing
  end

  vcs_type = detect_vcs(args.path)

  if vcs_type
    vcs_file = "#{args.project_path}/.idea/vcs.xml"
    relative_path = project_relative_path(args.path, args.project_path)

    template vcs_file do
      source 'project/vcs.xml'
      cookbook 'intellij'
      user args.user
      group args.group
      action :create_if_missing
    end

    xml_edit vcs_file do
      parent '/project/component[@name="VcsDirectoryMappings"]'
      target "/project/component[@name=\"VcsDirectoryMappings\"]/mapping[@directory=\"#{relative_path}\"]"
      fragment "<mapping directory=\"#{relative_path}\" vcs=\"#{vcs_type}\" />"
      not_if { vcs_type.to_s.empty? }
      action :append_if_missing
    end
  end

  if maven_module?(args.path)
    misc_file = "#{args.project_path}/.idea/misc.xml"
    relative_pom = project_relative_path("#{args.path}/pom.xml", args.project_path)

    template misc_file do
      source 'project/misc.xml'
      cookbook 'intellij'
      user args.user
      group args.group
      action :create_if_missing
    end

    create_missing_component('MavenProjectsManager', misc_file, '<option name="originalFiles"><list></list></option>')

    xml_edit misc_file do
      parent '/project/component[@name="MavenProjectsManager"]/option/list'
      target "/project/component[@name=\"MavenProjectsManager\"]/option/list/option[@value=\"#{relative_pom}\"]"
      fragment "<option value=\"#{relative_pom}\" />"
      action :append_if_missing
    end
  end
end

action :delete do
  args = @new_resource

  module_file, = generate_module_file_path(args.path, args.module_name, args.config_directory)

  file module_file do
    action :delete
  end

  relative_module_file = project_relative_path(module_file, args.project_path)
  relative_module = project_relative_path(args.path, args.project_path)
  relative_pom = project_relative_path("#{args.path}/pom.xml", args.project_path)

  {
    "#{args.project_path}/.idea/modules.xml" => "/project/component[@name=\"ProjectModuleManager\"]/modules/module[@filepath=\"#{relative_module_file}\"]",
    "#{args.project_path}/.idea/vcs.xml" => "/project/component[@name=\"VcsDirectoryMappings\"]/mapping[@directory=\"#{relative_module}\"]",
    "#{args.project_path}/.idea/misc.xml" => "/project/component[@name=\"MavenProjectsManager\"]/option/list/option[@value=\"#{relative_pom}\"]",
  }.each do |settings_file, target|
    xml_edit settings_file do
      target target
      action :remove
      only_if { ::File.exist?(settings_file) }
    end
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Module
end
