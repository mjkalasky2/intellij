resource_name :intellij
default_action :install

property :version, String
property :path, String, name_property: true
property :source, String
property :checksum, String
property :edition, Symbol, equal_to: [:ultimate, :community], default: :community
property :properties, Hash
property :user, String
property :group, String

action :install do
  source, checksum, version = summarize_release(@new_resource)
  path = @new_resource.path
  edition = @new_resource.edition
  user = @new_resource.user
  group = @new_resource.group
  properties = @new_resource.properties

  package_file = get_tmp_path(source)
  local_version, local_edition, = local_intellij_info(path)
  edition_code = edition_short_code(edition)

  directory path do
    recursive true
    user user
    group group
  end

  unless local_version.to_s.empty?
    log "The IntelliJ #{edition} edition in #{path} is already installed in the #{version} version" do
      level :info
    end if local_version == version && local_edition == edition_short_code(edition)

    log "The IntelliJ in #{path} is already installed, but does not match requirements (edition: #{edition_code} => #{local_edition}, version: #{version} => #{local_version})" do
      level :warn
    end if local_version != version || local_edition != edition_short_code(edition)
  end

  remote_file package_file do
    source source
    checksum checksum
    user user
    group group
    not_if { intellij_installed?(path) }
  end

  case ::File.extname(package_file)
  when '.zip'
    zipfile package_file do
      into path
      not_if { intellij_installed?(path) }
    end
  when '.gz', '.tar'
    tar_extract package_file do
      action :extract_local
      not_if { intellij_installed?(path) }
      target_dir path
      creates "#{path}/bin"
      tar_flags [ '--strip-components 1' ]
      user user
      group group
    end
  else
    raise IntelliJ::Exceptions::IntelliJArchiveTypeNotSupported, ::File.extname(package_file)
  end

  if properties
    java_properties "#{path}/bin/idea.properties" do
      properties properties
    end
  end
end

action :update do
  args = @new_resource
  _, _, version = summarize_release(args)
  path = args.path
  edition = args.edition
  local_version, local_edition, = local_intellij_info(path)

  if local_version != version || local_edition != edition_short_code(edition)
    log "Removing the previous IntelliJ #{local_version} #{local_edition} edition in #{path}" do
      level :info
    end unless local_version.to_s.empty?

    directory args.path do
      recursive true
      action :delete
    end

    intellij args.name do
      source args.source
      user args.user
      group args.group
      edition args.edition
      checksum args.checksum
      path args.path
      properties args.properties
      action :install
    end
  else
    log "The IntelliJ #{edition} edition in #{path} is already installed in the #{version} version" do
      level :info
    end
  end
end

action :delete do
  args = @new_resource
  Chef::Application.fatal!('You must pass either source or version parameter in order to remove IntelliJ!', 42) if !args.path && !args.version

  local_version, local_edition, = local_intellij_info(args.path)

  log "Removing the previous IntelliJ #{local_version} #{local_edition} edition in #{args.path}" do
    level :info
  end if local_version

  directory args.path do
    recursive true
    action :delete
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Install
end
