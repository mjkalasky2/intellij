resource_name :intellij_sdk
default_action :add

property :workspace, String, required: true
property :sdk_name, String, name_property: true
property :path, String
property :type, Symbol, equal_to: [:JavaSDK, :RUBY_SDK], default: :JavaSDK
property :roots_config, String
property :user, String
property :group, String

action :add do
  args = @new_resource
  jdk_table_file = "#{args.workspace}/config/options/jdk.table.xml"

  directory ::File.dirname(jdk_table_file) do
    recursive true
    user args.user
    group args.group
  end

  template jdk_table_file do
    source 'workspace/jdk.table.xml'
    user args.user
    group args.group
    cookbook 'intellij'
    action :create_if_missing
  end

  create_missing_component('ProjectJdkTable', jdk_table_file, nil, 'application')

  roots_config = args.roots_config || generate_roots_config(args.type, args.path)

  xml_edit jdk_table_file do
    parent '/application/component[@name="ProjectJdkTable"]'
    target "/application/component[@name=\"ProjectJdkTable\"]/jdk[name/@value=\"#{args.sdk_name}\"]"
    fragment <<-EOH
      <jdk version="2">
          <name value="#{args.name}" />
          <type value="#{args.type}" />
          <homePath value="#{args.path}" />
          #{roots_config}
      </jdk>
    EOH
    action :append_if_missing
  end
end

action :delete do
  args = @new_resource
  file = "#{args.workspace}/config/options/jdk.table.xml"

  xml_edit file do
    target "/application/component[@name=\"ProjectJdkTable\"]/jdk[name/@value=\"#{args.sdk_name}\"]"
    action :remove
    only_if { ::File.exist?(file) }
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Workspace::SDK
end
