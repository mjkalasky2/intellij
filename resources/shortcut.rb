resource_name :intellij_shortcut
default_action :create

property :intellij_path, String, name_property: true
property :directory, String
property :idea_properties, String
property :shortcut_name, String
property :user, String
property :group, String

action :create do
  args = @new_resource
  name = args.shortcut_name || default_shortcut_name(args.intellij_path)
  dir = args.directory || default_shortcut_directory(args.user)

  directory dir do
    recursive true
    user args.user
    group args.group
  end

  if Chef::Platform.windows?
    windows_shortcut "#{dir}/#{name}.lnk" do
      cwd "#{args.intellij_path}/bin/"
      target windows_shortcut_target(args.idea_properties, args.intellij_path)
      arguments windows_shortcut_arguments(args.idea_properties, args.intellij_path)
      iconlocation "#{args.intellij_path}/bin/idea.ico"
    end
  else
    template "#{dir}/#{name.tr(' ', '_')}.desktop" do
      source 'shortcut/.desktop.erb'
      user args.user
      group args.group
      variables(
        path: args.intellij_path,
        properties: args.idea_properties,
        name: name
      )
    end
  end
end

action :delete do
  args = @new_resource
  name = args.shortcut_name || default_shortcut_name(args.intellij_path)
  dir = args.directory || default_shortcut_directory(args.user)
  extension = Chef::Platform.windows? ? '.lnk' : '.desktop'
  file "#{dir}/#{name}#{extension}" do
    action :delete
  end
end

action_class do
  include IntelliJ::Shortcut
end
