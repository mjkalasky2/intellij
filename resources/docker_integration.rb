resource_name :intellij_docker_integration
default_action :create

property :workspace_path, String, required: true
property :machine_name, String
property :server_name, String, name_property: true
property :api_url, String
property :registries, Hash, callbacks: {
  'should have at least the address defined': IntelliJ::Validators::DockerIntegration.registry_address_defined?,
  'should not have different keys than address, email and username': IntelliJ::Validators::DockerIntegration.proper_registry_keys?,
}
property :shared_directories, Hash

property :user, String
property :group, String

action :create do
  args = new_resource

  directory args.workspace_path do
    action :create
    recursive true
    user args.user
    group args.group
  end

  directory "#{args.workspace_path}/config/options" do
    action :create
    recursive true
    user args.user
    group args.group
  end

  if args.server_name
    servers_file = "#{args.workspace_path}/config/options/remote-servers.xml"

    file servers_file do
      content '<application/>'
      user args.user
      group args.group
      action :create_if_missing
    end

    create_missing_component('RemoteServers', servers_file, nil, 'application')

    xml_edit servers_file do
      parent '/application/component[@name="RemoteServers"]'
      target "/application/component[@name=\"RemoteServers\"]/remote-server[@name=\"#{args.server_name}\"][@type=\"docker\"]"
      fragment build_server_xml(args.api_url, args.shared_directories, args.server_name, args.machine_name)
      action :append_if_missing
    end
  end

  if args.registries
    registry_file = "#{args.workspace_path}/config/options/docker-registry.xml"

    file registry_file do
      content '<application/>'
      user args.user
      group args.group
      action :create_if_missing
    end

    create_missing_component('DockerRegistry', registry_file, nil, 'application')

    xml_edit registry_file do
      edits generate_bulk_registries(args.registries)
      action :bulk
    end
  end
end

action :merge do
  args = new_resource
  data = load_existing_config(args.server_name, args.workspace_path)
  data[:registries].merge!(hash_sym_to_s(args.registries)) if args.registries
  data[:shared_directories].merge!(hash_sym_to_s(args.shared_directories)) if args.shared_directories
  data[:api_url] ||= args.api_url
  data[:machine_name] ||= args.machine_name

  intellij_docker_integration args.name do
    server_name args.server_name
    workspace_path args.workspace_path
    machine_name data[:machine_name]
    api_url data[:api_url]
    registries data[:registries]
    shared_directories data[:shared_directories]
    user args.user
    group args.group
  end
end

action :delete do
  args = new_resource
  servers_file = "#{args.workspace_path}/config/options/remote-servers.xml"

  xml_edit servers_file do
    target "/application/component[@name=\"RemoteServers\"]/remote-server[@name=\"#{args.server_name}\"][@type=\"docker\"]"
    action :remove
    only_if { ::File.exist?(servers_file) }
    not_if { args.server_name.to_s.empty? }
  end

  registry_file = "#{args.workspace_path}/config/options/docker-registry.xml"

  xml_edit registry_file do
    edits remove_bulk_registries(args.registries)
    not_if { args.registries.to_s.empty? }
    only_if { ::File.exist?(registry_file) }
    action :bulk
  end
end

action_class do
  include IntelliJ::DockerIntegration
end
