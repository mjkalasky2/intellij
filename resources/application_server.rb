resource_name :intellij_application_server
default_action :create

property :server_name, String, name_property: true
property :workspace_path, String, required: true
property :path, String
property :type, Symbol, equal_to: [:JBoss, :Tomcat], default: :Tomcat
property :user, String
property :group, String

action :create do
  args = @new_resource
  servers_file = "#{args.workspace_path}/config/options/javaeeAppServers.xml"

  directory ::File.dirname(servers_file) do
    recursive true
    user args.user
    group args.group
  end

  file servers_file do
    user args.user
    group args.group
    content '<application></application>'
    action :create_if_missing
  end

  create_missing_component('AppserversManager', servers_file, '<LibraryTable></LibraryTable>', 'application')
  edits = [
    {
      parent: '/application/component[@name="AppserversManager"]',
      target: "/application/component[@name=\"AppserversManager\"]/ServerDK[@NAME=\"#{args.server_name}\"]",
      fragment: build_xml(args.server_name, args.type, args.path),
      action: :append_if_missing,
    },
  ]

  edits.push(
    parent: '/application/component[@name="AppserversManager"]/LibraryTable',
    target: "/application/component[@name=\"AppserversManager\"]/LibraryTable/library[@name=\"#{args.server_name}\"]",
    fragment: build_server_libraries(args.server_name),
    action: :append_if_missing
  ) unless xpath_exists?(servers_file, "/application/component[@name=\"AppserversManager\"]/LibraryTable/library[@name=\"#{args.server_name}\"]")

  xml_edit servers_file do
    edits  edits
    action :bulk
  end
end

action :delete do
  args = @new_resource
  servers_file = "#{args.workspace_path}/config/options/javaeeAppServers.xml"

  xml_edit servers_file do
    edits  [
      {
        target: "/application/component[@name=\"AppserversManager\"]/ServerDK[@NAME=\"#{args.server_name}\"]",
        action: :remove,
      },
      {
        target: "/application/component[@name=\"AppserversManager\"]/LibraryTable/library[@name=\"#{args.server_name}\"]",
        action: :remove,
      },
    ]
    action :bulk
    only_if { ::File.exist?(servers_file) }
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Workspace::AppServer
end
