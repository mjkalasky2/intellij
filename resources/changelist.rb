resource_name :intellij_changelist
default_action :create

property :changelist_name, String, name_property: true
property :comment, String
property :files, [String, Array]
property :default, [TrueClass, FalseClass]
property :project_path, String, required: true
property :user, String
property :group, String

action :create do
  args = @new_resource
  workspace_file = "#{args.project_path}/.idea/workspace.xml"

  directory ::File.dirname(workspace_file) do
    recursive true
    user args.user
    group args.group
  end

  file workspace_file do
    content '<project version="4"></project>'
    user args.user
    group args.group
    action :create_if_missing
  end

  create_missing_component('ChangeListManager', workspace_file, default_manager)

  files = generate_files_list(args.files, args.project_path)
  data = get_changelist_data(args.project_path, args.changelist_name)
  data[:comment] = args.comment
  data[:default] = args.default
  data[:files] = files

  xml = generate_xml(data)

  bulk_actions = generate_bulk_actions(files, args.changelist_name, xml)

  xml_edit workspace_file do
    action :bulk
    edits bulk_actions
  end
end

action :update do
  args = @new_resource
  workspace_file = "#{args.project_path}/.idea/workspace.xml"

  directory ::File.dirname(workspace_file) do
    recursive true
    user args.user
    group args.group
  end

  file workspace_file do
    content '<project version="4"></project>'
    user args.user
    group args.group
    action :create_if_missing
  end

  create_missing_component('ChangeListManager', workspace_file, default_manager)

  files = generate_files_list(args.files, args.project_path)
  data = get_changelist_data(args.project_path, args.changelist_name)

  data[:comment] = args.comment if args.comment
  data[:default] = args.default if args.default
  data[:files] = files if args.files

  xml = generate_xml(data)

  bulk_actions = generate_bulk_actions(files, args.changelist_name, xml)

  xml_edit workspace_file do
    action :bulk
    edits bulk_actions
  end
end

action :delete do
  args = @new_resource
  file = "#{args.project_path}/.idea/workspace.xml"

  xml_edit file do
    action :remove
    target "/project/component[@name=\"ChangeListManager\"]/list[@name=\"#{args.changelist_name}\"]"
    only_if { ::File.exist?(file) }
  end
end

action :delete_files do
  args = @new_resource
  workspace_file = "#{args.project_path}/.idea/workspace.xml"

  directory ::File.dirname(workspace_file) do
    recursive true
    user args.user
    group args.group
  end

  file workspace_file do
    content '<project version="4"></project>'
    user args.user
    group args.group
    action :create_if_missing
  end

  create_missing_component('ChangeListManager', workspace_file, default_manager)

  files = generate_files_list(args.files, args.project_path)

  data = get_changelist_data(args.project_path, args.changelist_name)

  data[:comment] = args.comment if args.comment
  data[:default] = args.default if args.default
  data[:files] = data[:files] - files

  xml = generate_xml(data)

  bulk_actions = generate_bulk_actions(files, args.changelist_name, xml)

  xml_edit workspace_file do
    action :bulk
    edits bulk_actions
  end
end

action :add_files do
  args = @new_resource
  workspace_file = "#{args.project_path}/.idea/workspace.xml"

  directory ::File.dirname(workspace_file) do
    recursive true
    user args.user
    group args.group
  end

  file workspace_file do
    content '<project version="4"></project>'
    user args.user
    group args.group
    action :create_if_missing
  end

  create_missing_component('ChangeListManager', workspace_file, default_manager)

  files = generate_files_list(args.files, args.project_path)

  data = get_changelist_data(args.project_path, args.changelist_name)

  data[:comment] = args.comment if args.comment
  data[:default] = args.default if args.default
  data[:files] = data[:files] | files

  xml = generate_xml(data)

  bulk_actions = generate_bulk_actions(files, args.changelist_name, xml)

  xml_edit workspace_file do
    action :bulk
    edits bulk_actions
  end
end

action_class do
  include IntelliJ::Workspace::Changelist
end
