resource_name :intellij_plugin
default_action :install

property :plugin_name, String, name_property: true
property :workspace, String, required: true
property :intellij_path, String
property :source, String
property :update, Integer
property :edition, Symbol, equal_to: [:ultimate, :community], default: :community
property :repository_filename, String
property :user, String
property :group, String

action :install do
  args = @new_resource

  raise IntelliJ::Exceptions::IntelliJNotFound, args.intellij_path if args.intellij_path && !::File.exist?(args.intellij_path)

  repo = "#{args.workspace}/config/plugins"
  source = args.source

  if source
    log "Installing the plugin #{args.plugin_name} from specific source: #{args.source}" do
      level :info
    end
  else
    plugin_data = plugin_data_from_repository(args.plugin_name, args.edition, args.update, args.intellij_path)
    intellij_version = "IntelliJ #{plugin_data['edition']} edition #{plugin_data['intellij_build']}".strip

    log "Installing the plugin #{args.plugin_name} for #{intellij_version} from IntelliJ's repository" do
      level :info
    end

    source = plugin_data['source']
  end

  path = "#{repo}/#{::File.basename(URI.parse(source).path)}"

  directory repo do
    recursive true
    user args.user
    group args.group
  end

  unless plugin_installed?(repo, source, args.repository_filename)
    remote_file path do
      source source
      user args.user
      group args.group
    end

    zipfile path do
      into repo
      overwrite true
      not_if do
        contents = zip_contents(path)
        (!contents.empty? && ::File.exist?("#{repo}/#{contents.first}")).tap do |check|
          Chef::Log.info("Skipping unzipping the plugin since it is already installed in #{"#{repo}/#{contents.first}"}'") if check
        end
      end
      only_if { ::File.extname(path) == '.zip' }
    end

    file path do
      action :delete
      only_if { ::File.extname(path) == '.zip' }
    end
  end
end

action :update do
  args = @new_resource

  raise IntelliJ::Exceptions::IntelliJNotFound, args.intellij_path if args.intellij_path && !::File.exist?(args.intellij_path)

  repo = "#{args.workspace}/config/plugins"
  source = args.source

  if source
    log "Installing the plugin #{args.plugin_name} from specific source: #{args.source}" do
      level :info
    end
  else
    plugin_data = plugin_data_from_repository(args.plugin_name, args.edition, args.update, args.intellij_path)
    intellij_version = "IntelliJ #{plugin_data['edition']} edition #{plugin_data['intellij_build']}".strip

    log "Installing the plugin #{args.plugin_name} for #{intellij_version} from IntelliJ's repository" do
      level :info
    end

    source = plugin_data['source']
  end

  path = "#{repo}/#{::File.basename(URI.parse(source).path)}"

  directory repo do
    recursive true
    user args.user
    group args.group
  end

  remote_file path do
    source source
    user args.user
    group args.group
  end

  if ::File.extname(path) == '.zip'
    zipfile path do
      into repo
      overwrite true
    end

    file path do
      action :delete
    end
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Plugins
end
