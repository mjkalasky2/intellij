resource_name :intellij_workspace
default_action :create

property :intellij_path, String
property :path, String, name_property: true
property :default_project, String
property :color_scheme, Symbol, equal_to: [:IntelliJ, :Darcula]
property :code_style, String
property :code_style_variables, Hash
property :user, String
property :group, String

action :create do
  args = @new_resource

  directory args.path do
    recursive true
    user args.user
    group args.group
  end

  directory "#{args.path}/config" do
    recursive true
    user args.user
    group args.group
  end

  properties_file = "#{args.path}/config/idea.properties"
  main_properties_file = "#{args.intellij_path}/bin/idea.properties"

  file properties_file do
    content ::File.exist?(main_properties_file) ? ::File.read(main_properties_file) : ''
    user args.user
    group args.group
  end

  java_properties properties_file do
    properties(
      'idea.config.path': "#{args.path}/config"
    )
  end

  directory "#{args.path}/config/options" do
    recursive true
    user args.user
    group args.group
  end

  if args.default_project
    Chef::Log.info("Setting up the default project as #{args.default_project}")
    recent_projects_file = "#{args.path}/config/options/recentProjects.xml"

    template recent_projects_file do
      source 'workspace/recentProjects.xml'
      cookbook 'intellij'
      user args.user
      group args.group
      action :create_if_missing
    end

    create_missing_component('RecentProjectsManager', recent_projects_file, nil, 'application')

    xml_edit recent_projects_file do
      edits generate_recent_projects_bulk(args.default_project)
      action :bulk
    end
  end

  if args.color_scheme
    color_file = "#{args.path}/config/options/colors.scheme.xml"
    laf_file = "#{args.path}/config/options/laf.xml"

    template color_file do
      source 'workspace/colors.scheme.xml'
      cookbook 'intellij'
      user args.user
      group args.group
      action :create_if_missing
    end

    template laf_file do
      source 'workspace/laf.xml'
      cookbook 'intellij'
      user args.user
      group args.group
      action :create_if_missing
    end

    xml_edit color_file do
      parent '/application/component[@name="EditorColorsManagerImpl"]'
      target '/application/component[@name="EditorColorsManagerImpl"]/global_color_scheme'
      fragment "<global_color_scheme name=\"#{args.color_scheme}\" />"
      action :append_if_missing
    end

    xml_edit laf_file do
      parent '/application/component[@name="LafManager"]'
      target '/application/component[@name="LafManager"]/laf'
      fragment get_color_class(args.color_scheme)
      action :append_if_missing
    end
  end

  if args.code_style
    directory "#{args.path}/config/codestyles" do
      recursive true
      user args.user
      group args.group
    end

    template "#{args.path}/config/codestyles/Default.xml" do
      source args.code_style
      variables args.code_style_variables
      user args.user
      group args.group
    end
  end
end

action :delete do
  args = @new_resource

  %W(#{args.path}/config #{args.path}/system).each do |dir|
    directory dir do
      recursive true
      action :delete
    end
  end
end

action_class do
  include IntelliJ::Workspace
end
