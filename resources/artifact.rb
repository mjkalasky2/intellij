resource_name :intellij_artifact
default_action :create

property :artifact_name, String, name_property: true
property :project_path, String, required: true
property :output_path, String
property :root, String
property :type, String
property :user, String
property :group, String

action :create do
  args = @new_resource
  xml = build_xml(args.artifact_name, args.type, args.output_path, args.root, args.project_path)
  artifact_path = get_artifact_path(args.project_path, args.artifact_name)

  directory ::File.dirname(artifact_path) do
    recursive true
    user args.user
    group args.group
  end

  file 'Update artifact configuration' do
    content pretty_format_xml(xml)
    path artifact_path
    user args.user
    group args.group
  end
end

action :delete do
  args = @new_resource
  artifact_path = get_artifact_path(args.project_path, args.artifact_name)

  file artifact_path do
    action :delete
  end
end

action_class do
  include IntelliJ::Helpers
  include IntelliJ::Workspace::Artifact
end
