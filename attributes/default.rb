default['intellij']['recipe']['path'] = 'C:/Program Files/JetBrains/IntelliJ'
default['intellij']['recipe']['source'] = nil
default['intellij']['recipe']['checksum'] = nil
default['intellij']['recipe']['edition'] = :community
default['intellij']['recipe']['version'] = nil
default['intellij']['recipe']['user'] = nil
default['intellij']['recipe']['group'] = nil
